## @file mainpage.py
#
#
#
#   @mainpage
#
#   
#
#   @section sec_port Portfolio Overview
#   This is a digital portfolio for ME305, a class revolving around mechatronics and the application of python  
#   to accomplish tasks formally known to only be possible with the tricks and kicks of assembly language.
#   See individual modules for details.
#
#
#
#  Included modules are:
#  * Hw0x00 (\ref page_elevator)
#  * Lab0x00 (\ref page_lab0)
#  * Lab0x01 (\ref page_fib)
#  * Lab0x02 (\ref page_lab2)
#  * Lab0x03 (\ref page_Nucleo)
#  * Lab0x04 (\ref page_Lab4)
#  * Lab0x05 (\ref page_LEDBLE)
#  * Lab0x06 (\ref page_Lab6)
#
#
#   @author Mark Edin
#  
#
#
#   @page page_lab0 Lab 0
#
#   Here is the link to my Lab 0 submission video:
#   https://drive.google.com/file/d/1YhmmBYjybgUexM2TZQTyE1xL1VEpSaVU/view?usp=sharing
#   
#
#   @page page_fib Fibonacci Calculator
#
#   You can find the source code for the Fibonacci Sequence Calculator here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab1/FibonacciCalc.py
#
#
#   @page page_elevator ElevatorFSM
#
#   @section page_elevator ElevatorFSM Source Code Access 
#   You can find the source code for the elevator finite state machine program here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Hw0x00/Hw0x00TaskElevator.py
#   
#
#   
#
#   @section page_elevator Documentation
#   The documentation for the Elevator Finite State Machine program is located here: \ref ElevatorFSM
#   And the finite state machine is located below:
#
#   @image html ElevatorFSM_Hw0x00.png
#
#   @page page_lab2 Lab 2
#
#   @section page_lab2 Nucleo blinking LED Source Code Access 
#   You can find the source code for the Nucleo blinking LED finite state machine program here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/lab2b/nucleoclasseslab2.py
#
#   And the source code for the Blinking LED FSM's main.py file is located here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/lab2b/nucleomain.py
#
#   @section page_lab2 Documentation
#   The documentation for the Nucleo blinking LED finite state machine program is located in the files folder labeled nucleoclasseslab2.Lab2
#   
#   
#
#   And the State Diagram for the vLEDFSM is here:
#   @image html StateDiagramVLED.png
#
#   And the State Diagram for the Real LED FSM is below:
#   @image html StateDiagramRealLED.png
#   
#   
#   @page page_Nucleo Nucleo Encoder
#
#   @section page_Nucleo Nucleo_Encoder Source Code Access 
#   You can find the source code for the Nucleo Encoder: 
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab4/nucencoder.py
#
#   @section page_Nucleo Documentation
#   The documentation for the encoder for the nucleo is located in the files fold labeled nucencoder.Encoder
#
#   
#   @page page_Lab4 UI Data Generator
#
#   @section page_Lab4 Data Generator Source Code Access 
#   You can find the source code and the task and state diagrams for the UI Data Generator at: 
#   https://bitbucket.org/edinmjr/me305_labs/src/master/ActualLab4/UI_dataGen.py
#
#   The source code for the Front end running on the PC is located here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/ActualLab4/UI_FrontLab4.py
#
#   The source code for the main.py running the tasks is located at:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/ActualLab4/main.py
#
#   @section page_Lab4 Documentation
#   The documentation for the Data Generator is located in the files folder labeled UI_FrontLab4.UI
#
#   The Task Diagram for Lab 4 is located here:
#   @image html TaskDiagramLab4.png
#
#   The State Diagram for the DataGen task is located below:
#   @image html StateDiagramDataGen.png
#
#   The State Diagram for the UI Front end is located below:
#   @image html StateDiagramUI.png
#
#   
#   @page page_LEDBLE Bluetooth LED Control
#
#   @section page_LEDBLE Bluetooth LED Control Source Code Access 
#   You can find the source code for the BLEModeule is here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab%205/Lab5classes.py
#
#   The source code for the LED Controller is located here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab%205/LEDController.py
#
#
#
#   and you can download the application here:
#   https://x.thunkable.com/copy/bab6ecb2dc74e4113e3fb07b5f8eab08
#
#   @section page_LEDBLE_doc Documentation
#   The documentation for the Bluetooth Module is located in the files folder labeled Lab5classes.Lab5
#   
#   The documentation for the LED Controller is located in the files folder labeled LEDController.Lab5
#
#   The task diagram for Lab 5 is located below, followed by the State Diagrams for the Bluetooth module and LED Controller:
#
#   @image html taskdiagram.png
#
#   @image html ledcontrollerstatediagram.png
#   
#   @image html blemoduletaskdiagram.png
#
#
#
#
#
#   @page page_Lab6 Closed Loop Speed Controller
#
#   @section page_Lab6 Closed Loop Speed Controller Source Code Access
#   
#   You can find the source code for the ClosedLoopModule here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/ClosedLoopModule.py
#
#   You can find the source code for the Controller task here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/Controller.py
#
#   You can find the source code for the User Task here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/TaskUser.py
#
#   You can find the source code for the main.py holding the Controller and User tasks here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/main.py
#
#   You can find the source code for the MotorDriver.py here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/MotorDriver.py
#
#
#  And finally, you can find the source code for the UI Front end running on the PC
#  and the main.py file to run the Front end here:
#  https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/maintestUI.py
#  https://bitbucket.org/edinmjr/me305_labs/src/master/Lab6/UI_FrontLab6.py
#
#
#   @section page_Lab6 Documentation
#
#   The task diagram for the Closed Loop Speed Controller is located below:
#   @image html TaskDiagramLab6.png
#
#   The State Diagram for the Controller is located here:
#   @image html StateDiagramController.png
#
#   The State Diagram for the Front End is located here:
#   @image html StateDiagramFrontEnd.png
#
#   The State Diagram for the User Task is located here:
#   @image html StateDiagramTaskUser.png
#
#
#   @section page_Lab6 Tuning Kp
#
#   With a Kp value of 0.15 and an Oref of 60, the following step response plot was generated:
#   @image html Kp0.15.png
#
#   With a Kp Value of 0.1 and an Oref of 60, the following step response plot was generated:
#   @image html Kp0.1.png
#
#   With a Kp Value of 0.08 and an Oref of 60, the following step response plot was generated:
#   @image html Kp0.8.png
#
#   It seems like the lowest value of Kp tested, which was 0.08, reached the desired speed the quickest
#   and the most efficeintly (the less oscillations the better).
#
#
#
#