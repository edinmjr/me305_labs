# -*- coding: utf-8 -*-
"""
@file UI_dataGen.py

@author Mark Edin
"""

import utime
import array as arr
from pyb import UART


class UI_dataGenTask:
    '''
    @brief A task class that runs on the nucleo while connected to the motor encoder. 
    @details This class allows the nucleo to recieve commands from the front end UI running in spyder through the serial port, 
    and performs different operations depending on what character is recieved. When 'g' is send from the UI, the task begins collecting encoder
    position data and appends the items to an array along with the current timestamp. Until 's' is pressed in the UI (and sent over the serial port), 
    or 10 seconds has passed (whichever comes first), the task 'run' will collect data. When one of those two events happen, the data is sent back
    to the UI through the serial port, and the 'run' task waits to recieve another command. 
    '''  
     ## Initialization state
    S0_INIT             = 0  
    ## Wait for command state
    S1_WAIT_FOR_USR_INPT     = 1 
    ## Collect Data
    S2_COLL_DATA  = 2

    def __init__(self, taskNum, interval, EncoderDriver, dbg=False):
        
        ## The name of the task
        self.taskNum = taskNum 
        ##  The sample frequency of the encoder
        self.interval = int(1E3/interval)              
        ## Flag to print debug messages or supress them
        self.dbg = dbg    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()   
         ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)    
        ## The position array used to store data gathered from the encoder during each task iteration   
        self.posArray = arr.array('l', [])        
        ## The time array used to store data gathered from the encoder during each task iteration
        self.timArray = arr.array('l', [])                
        ## A variable used to store the current position of the encoder
        self.position = 0        
        ## A class "copy" of the encoder driver object
        self.EncoderDriver = EncoderDriver        
        ## A variable used to store commands sent by the UI through the serial port
        self.cmd = 0              
        ## Class copy of the UART object used to communicate with the UI through the serial port 
        self.myuart = UART(2)            
        ## Debugging
        if self.dbg:
            print('Created scaler task')          
            
    def run(self, interval):
        '''
        @brief Runs one iteration of the task
        @details Runs one iteration of the task which updates arrays with the current encoder position/time if the 'g' key was pressed in the UI. 
        The serial port is constantly being checked for characters sent by the UI, and if the 'g' key is pressed, data collection begins. The task
        collects data for 10 seconds or until the 's' key is pressed in the UI. Once one of those events happens, the data is sent from this task to
        the UI running in spyder through the serial port. Once the data transfer is complete, all relevant variables are cleared in the S0_INIT state
        and the task begins to wait for the "start" command from the UI ('g' key press). 
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:   
            if(self.state == self.S0_INIT):
                #self.printTrace()                
                #run state 0 code
                self.EncoderDriver.zero()
                self.posArray = []
                self.timArray = []
                self.transitionTo(self.S1_WAIT_FOR_USR_INPT)   
                
            elif(self.state == self.S1_WAIT_FOR_USR_INPT):
                #self.printTrace()
                #Run state 1 code
                if self.myuart.any() != 0:
                    self.cmd = self.myuart.readchar()
                    if self.cmd == 103:
                        self.coltime = utime.ticks_ms()
                        self.transitionTo(self.S2_COLL_DATA)
                        
                        
                        
            elif(self.state == self.S2_COLL_DATA):
                #self.printTrace()
                #state 2 data here:                   
                self.EncoderDriver.update()
                self.position = self.EncoderDriver.getPosition()
                if(self.cmd == 103 and len(self.posArray) < (10*interval)):
                    self.posArray.append(self.position*9)
                    self.timArray.append(utime.ticks_diff(self.curr_time, self.coltime))
                    if self.myuart.any() != 0:
                        self.cmd = self.myuart.readchar()                    
                elif(self.cmd == 115 or len(self.posArray) == (10*interval)):
                    self.cmd = None
                    for n in range(len(self.posArray)):
                        print('{:},{:}'.format(self.timArray[n], self.posArray[n]))
                    self.transitionTo(self.S0_INIT)                
                else:
                    pass
                                              

            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
            
           
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
