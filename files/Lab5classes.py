# -*- coding: utf-8 -*-
"""
@file Lab5classes.py
@author: edinm
@package Lab5classes
"""
from pyb import UART
import utime
import shares



class BLEModuleDriver:
    
    S0_INIT = 0
    
    S1_WAIT_FOR_INPUT = 1
    
    S2_LED_PATTERN = 2
    
    
    
    def __init__(self, taskNum, interval, dbg=True):
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The name of the task
        self.taskNum = taskNum
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()  
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval) 
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        
        ##Uart object
        self.myuart = UART(3, baudrate = 9600)
        
        self.cmd = None
        
        self.curr_time = 0
        
        #self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        #self.LEDController = LEDController
        
        ## Debugging
        if self.dbg:
            print('Created scaler task: BLEModule') 
            
        self.cmd = None
        
        self.check = True
        
    def run(self):
        '''
        @brief Runs one interation of the task 
        @details
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0: 
            if(self.state == self.S0_INIT):
                self.printTrace()
                #run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
                
                
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                self.printTrace()
                #shares.cmd = None
                #run state 1 code
                if self.myuart.any() != 0:
                    #sanitize cmd
                    try:
                        self.check = True
                        self.cmd = int((self.myuart.readline()))
                        self.myuart.write('Updating Frequency of LED to ' + str(self.cmd) + '\r\n')
                        int(100/self.cmd)
                    except(ValueError, TypeError, ZeroDivisionError):                       
                        self.myuart.write('Please enter a valid non-zero integer')
                        self.cmd = None
                        shares.cmd = None
                        self.check = False
                        
                    if self.check == True:    
                        self.transitionTo(self.S2_LED_PATTERN)                         
                    elif self.check == False:
                        pass
                    
                    
                    
                
            elif(self.state == self.S2_LED_PATTERN):
                self.printTrace()
                #run state 2 code               
                shares.cmd = self.cmd             
                self.transitionTo(self.S1_WAIT_FOR_INPUT)
                    

                
                
            else:
                pass
        
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    # def ledPattern(self, freq):

    #     pass           
                    
                    
    def transitionTo(self, newState):
            '''
            @brief      Updates the variable defining the next state to run
            '''
            self.state = newState          
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)         