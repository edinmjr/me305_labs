'''
@file       FibonacciCalc.py
@brief      A file that calculates the fibonnaci sequence number requested. 
@details    Use "fib(#)" to fund the Fibonacci seqeunce numebr at index #
@package FibonacciCalc
@author     Mark Edin

'''


class Fibonacci:
    '''
    @brief      A class representing a fibonacci sequence number finder
    @details    This class represents the fibonacci sequence numebr calculator. 
    
    '''
    def __init__(idx): #This is the constructor
    
        '''
    @brief This constructor runs when a FibonacciCalc object is created
    @details The fibonacciCalc is constructed with no initial conditions
        '''
        idx.fib()
        
        
    def fib ():
        '''
            @brief This function takes the input (idx) and finds the associated fibonacci sequence number 
            @details The input of (idx) will return the associated fibonacci sequence number requested. The user will then be asked if they would like to find another number. 
            The function runs until the user enters "0" when they are prompted if they would like to find another number.  
            '''
        yres = True
        while yres == True:
            n1 = 0
            n2 = 1
            counter = 0
            idx = (input("Enter Fibonnaci Sequence Number: "))
            print('Calculating Fibonacci number at index n = {:}.'.format(idx))
            
            try:
                idx = int(idx)
                while counter <= idx:
                    nterm = n1 + n2
                    n1 = n2
                    n2 = nterm
                    counter += 1
                
                    if idx == 0 or idx == 1:
                        while yres == True:
                            restart = input("Would you like to enter another number? 1 = Yes, 0 = No: ")
                            if restart not in ('1', '0'):
                                print('Invalid Input')
                            elif restart == '1':
                                break
                            elif restart == '0':
                                print ('Thanks for using my program!')
                                yres = False
                                
                    elif counter == idx:
                        print('Fibonacci sequence number requested: ', n1)
                        while yres == True:
                            restart = input("Would you like to enter another number? 1 = Yes, 0 = No: ")
                            if restart not in ('1', '0'):
                                print('Invalid Input')
                            elif restart == '1':
                                break
                            elif restart == '0':
                                print ('Thanks for using my program!')
                                yres = False
            except:
                print('Please enter a positive integer')
            
    
    fib()

    