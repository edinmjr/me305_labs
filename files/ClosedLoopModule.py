# -*- coding: utf-8 -*-
"""
@file ClosedLoopModule.py
@author Mark Edin
@package ClosedLoopModule
"""


class ClosedLoop:
    '''
    @brief A module that takes in the measured and given values and calculates a new duty cycle
    @details This module takes in the reference velocity, the real motor speed, and calculates the new duty cycle
    step increase in order to reach the desired velocity. it also includes a method to Set the Kp value, and 
    a method to get the Kp value. 
    '''
   
    
    def __init__(self, Kp):
        '''
        @brief This constructor runs when a ClosedLoopModule object is created
        @details The ClosedLoopModule is constructed with an intial Kp value, but 
        it is later changed to the value that is determined from the input on the Front End running
        on the PC. 
        '''
        ## Local initial Kp value, it is changed to whatever is sent from the front end
        self.Kp = float(Kp)
        ## Local Oref value, it is manipulated by the update() function
        self.Oref = 0
        ## Local Oreal value, it is manipulated by the update() function
        self.Oreal = 0
        
    

    def update(self, Oref, Oreal):
        '''
        @brief Takes in Oref and Oreal and sets them to the local values. Performs new duty cycle step calculation
        and returns that number.
        '''
        
        self.Oref = Oref
        self.Oreal = Oreal
        
        duty = self.Kp * (self.Oref-self.Oreal)
        return duty
    
    def get_Kp(self):
        '''
        @brief Gets the value of Kp
        '''
        
        return self.Kp
    
    def set_Kp(self, Kp):
        '''
        @brief Sets the value of Kp to be used in duty cycle step increase calculations
        '''
        self.Kp = Kp
    

        
        
    
    
    