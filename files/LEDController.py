# -*- coding: utf-8 -*-
"""
@file LEDController.py
@package LEDController
@author: edinm
"""

import utime
import pyb
import shares


class LEDController:
    
    S0_INIT = 0
    
    S1_LED_ON = 1
    
    S2_LED_OFF = 2
    
    
    def __init__(self, taskNum, interval, dbg=True):
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The name of the task
        self.taskNum = taskNum
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The interval of time, in freq, between runs of the task
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
              
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        
        ##Uart object
        #self.myuart = UART(3, baudrate = 9600)
        
        self.cmd = None
        
        self.curr_time = 0
        
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## Debugging
        if self.dbg:
            print('Created scaler task: LEDController') 
        
    def run(self):
        '''
        @brief
        @details
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                self.printTrace()
                #run state 0 code
                self.transitionTo(self.S1_LED_ON)
                
                
            elif(self.state == self.S1_LED_ON): 
                self.printTrace()
                #run state 1 code
                self.updateFreq(shares.cmd)
                self.pinA5.high()
                self.transitionTo(self.S2_LED_OFF)
                    
            elif(self.state == self.S2_LED_OFF): 
                self.printTrace()
                #run state 2 code
                self.updateFreq(shares.cmd)
                self.pinA5.low()
                self.transitionTo(self.S1_LED_ON)
                                      
            else:
                pass
            
            self.runs += 1
             # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
        
        
    def updateFreq(self, freq):
        '''
        @brief Updates the blink frequency of the LED
        '''
        if shares.cmd != None:
            self.interval = int(1000/freq)
            shares.cmd = None
        else:
            pass    
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState          
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)         