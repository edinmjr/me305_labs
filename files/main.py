"""
@file main.py
@brief A main.py file to run the datagen task.
@details This main.py holds the encoder setup code as well as the encoder driver object that is passed in to
the UI_dataGenTask. The task will run constantly, since it is in a Task like that is called within a "while True:"
loop. 
@author Mark Edin
"""
from UI_dataGen import UI_dataGenTask   
from nucencoder import EncoderDriver
import pyb


    
## Motor Object

PA6 = pyb.Pin.cpu.A6
PA7 = pyb.Pin.cpu.A7
tim = pyb.Timer(3, prescaler = 0, period = 0xffff)
#Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PA6, mode = pyb.Timer.ENC_AB)      
#Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PA7, mode = pyb.Timer.ENC_AB)
##EncoderDriver Obj
Encoder = EncoderDriver(PA6, PA7, tim, 0xffff)

interval = 5

## Task object
task0 = UI_dataGenTask(0, interval, Encoder, dbg=False)

taskList = [task0]

while True:
    for task in taskList:
        task.run(interval)
