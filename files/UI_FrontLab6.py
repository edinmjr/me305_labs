# -*- coding: utf-8 -*-
"""
@file UI_FrontLab6.py
@author Mark Edin
@package UI_FrontLab6
"""

import matplotlib.pyplot as plt
import time
import serial
import keyboard





class UI_Task:
    '''
    @brief A UI task built in python designed to send commands to a nucleo microcontroller
    @details A UI built in python designed to send Kp and reference speed values to the TaskUser located on the nucleo.
    After sending the data, it waits for 5 seconds for the desired motor speed to be reached, and then receives the speed and time
    data sent from the nucleo. It then plots that data in order to create a step response plot. It asks for a Kp value, how many reference
    values should be used, and then what reference values to use.
    '''
    
    ## Initialization state
    S0_INIT = 0
    ## Wait for character ('g') state
    S1_WAIT_FOR_CHAR = 1
    ## Enter in data to be sent to the nucleo state
    S2_ENTER_DATA = 2
    ## Wait for the data to be generated and sent from the nucleo state
    S3_WAIT_FOR_DATA = 3
    ## Process the data received from the nucleo to generate step response plot state
    S4_PROCESS_DATA = 4
    
    def __init__(self, taskNum, interval, dbg=False):
        '''
        @brief This constructor runs when a UI_Task object is created
        @details The UI_Task is contructed with an intial task number, a run interval, and a debug boolean statement. 
        '''
        ## The number of the task
        self.taskNum = taskNum
        ##  The amount of time in nanoseconds between runs of the task
        self.interval = int(interval*1E9)
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## The timestamp for the first iteration
        self.start_time = time.perf_counter_ns()
        ## Counter that describes the number of times the task has run
        self.runs = 0
        ## The current time variable
        self.curr_time = 0
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        ## the difference in current time vs. next time
        self.diff_time = self.curr_time - self.next_time
        ##Serial Port initializatoin
        self.ser = serial.Serial(port = 'COM3', baudrate = 115200, timeout = 1)
        ## An array used to hold position data
        self.position = []
        ## An array used to hold time data
        self.time = []
        ## An array used to hold data read from the serial port that was sent from the nucleo
        self.readData = []
        ## Debugging
        if self.dbg:
            print('Created user interface task')
        ## The command variable that is set based off of what the user inputs
        self.g = None
        ## A variable that holds 1 line of data that was read from the serial port
        self.line_string = None
        ## The list of line_strings read from the serial port
        self.line_list = []
        ## A variable used to hold the true/false value of the input timeout loop
        self.x = 0
        ## Local variable to hold number of ref speed values to be sent to nucleo
        self.n = 0
        ## Local cmd variable to deal with inputs
        self.cmd = []
        ## Speed data array
        self.speed = []
        ## Time data array
        self.time = []
        ## Time data str to be converted to float then int
        self.timestr = 0
        ## Speed data str to be converted to float then int
        self.speedstr = 0
        
    def run(self):
        '''
        @brief Runs one interation of the python UI task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = time.perf_counter_ns()
        if(self.start_time - self.interval >= 0):
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                self.speed = []
                self.time = []
                self.readData = []
                self.g = None
                #Run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                #Run State 1 Code
                print("Press [g] to enter Kp and a list of desired speeds")
                self.a = time.time()
                self.x = True
                while self.x:
                    if ((keyboard.is_pressed('g')) or (time.time() > self.a + 1000)):
                        self.x = False
                        self.g = 'g'  
                if self.g == 'g':
                    self.transitionTo(self.S2_ENTER_DATA)
                    self.g = None
                    #self.t0 = time.time()
                    
                    
            elif(self.state == self.S2_ENTER_DATA):
                self.printTrace()
                #Run state 2 code
                self.usrKp = float(input("Please enter your desired Kp value: "))           
                self.cmd.append(self.usrKp)
                self.n = int(input("Please enter the number of Reference Speeds you would like to test: "))
                for self.i in range(0, self.n):
                   self.refspeed = int((input("Please enter a reference speed # " + str(self.i + 1) + " : ")))               
                   self.cmd.append(self.refspeed)
                #print(self.cmd)
                self.ser.write('{:f},{:f}\r\n'.format(self.cmd[0], self.cmd[1]).encode('ascii'))
                self.transitionTo(self.S3_WAIT_FOR_DATA)
                self.cmd = None
                
                
            elif(self.state == self.S3_WAIT_FOR_DATA):
                self.printTrace()
                time.sleep(5)
                self.transitionTo(self.S4_PROCESS_DATA)      
                
            
            elif(self.state == self.S4_PROCESS_DATA):
                self.printTrace()
                #Run state 3 code
                print('Responses in waiting: ' + str(self.ser.in_waiting))                    
                while self.ser.in_waiting > 0:
                    self.line_string = self.ser.readline().decode('ascii')
                    self.line_list = self.line_string.strip('\r\n').split(',')  
                    self.timestr = self.line_list[0]
                    self.speedstr = self.line_list[1]
                    self.time.append(int(float(self.timestr)))
                    self.speed.append(int(float(self.speedstr)))
                #Plot the data from self.time and self.position
                #del self.time[0:5]
                #del self.speed[0:5]
                plt.plot(self.time, self.speed)
                #format axis
                plt.xlabel('Time (us)')
                plt.ylabel('Speed (Rad/s)')
                plt.title('Speed vs. Time')
                plt.grid(True)
                plt.show()
                self.transitionTo(self.S1_WAIT_FOR_CHAR)       
            else:
                pass 
            self.runs += 1           
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
           
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState
        
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
            
            
