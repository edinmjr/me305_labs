# -*- coding: utf-8 -*-
"""
@file Lab5classes.py
@author: edinm
@package Lab5classes
"""
from pyb import UART
import utime
import shares5



class BLEModuleDriver:
    '''
    @class BLEModuleDriver
    @brief This class contains the BLEModuleDriver
    @details This class contains all the functions needed to transmit and receive data from the nucleo using an HM-19 BLE module purchased from amazon. It should be 
    known that all testing was done without the enable (EN) pin being connected, because for some reason our nucleo's were unable to use the connected device 
    correctly. Also, data was sent and received by using the LightBlue app for iPhone. It is a bluetooth terminal that is able to send specific commands over
    bluetooth that are read by the nucleos serial port. 
    '''  
    ## Init state
    S0_INIT = 0
    ## Wait for input state
    S1_WAIT_FOR_INPUT = 1
    ## led patter state
    S2_LED_PATTERN = 2
    
    
    
    def __init__(self, taskNum, interval, dbg=True):
        '''
        @class BLEModuleDriver
        @brief This constructer runs once when a BLEModuleDriver object is created
        @details This constructor is ran when a BLEModuleDriver object is created, and initializes all of the local variables that are needed in order 
        to run the BLEModuleDriver task. 
        @param[in] The task number passed through the main represents which task this is in the dbg statement, the interval passed through the main is the 
        amount of time in ms that task run will be ran at, and the dbg boolean value is passed through the main (T = On etc.).
        '''  
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## The name of the task
        self.taskNum = taskNum
        ## A counter showing the number of times the task has run
        self.runs = 0
        ## The interval of time, in milliseconds, between runs of the task
        self.interval = int(interval)
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()  
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)  
        ## Flag to print debug messages or supress them
        self.dbg = dbg  
        ##Uart object
        self.myuart = UART(3, baudrate = 9600)
        ## local command variable assigned to BLE command
        self.cmd = None
        ## Current timestamp initialization
        self.curr_time = 0
        ## Debugging
        if self.dbg:
            print('Created scaler task: BLEModule') 
        ## Local variable used as a check to see if the task should move on to state 2
        self.check = True
        
    def run(self):
        '''
        @brief Runs one interation of the task 
        @details The task moves from state 1 to state 2 depending on if a value sent over bluetooth has been received by the nucleo. It then sends a BLE string
        back to the iPhone showing what the LED frequency was updated to. Input sanitization occurs and only allows positive integers to be entered. Once a valid
        input has been received over BLE, a shared file value is set to that specific frequency and the LED controller task is able to update the speed. 
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0: 
            if(self.state == self.S0_INIT):
                self.printTrace()
                #run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_INPUT)              
                
            elif(self.state == self.S1_WAIT_FOR_INPUT):
                self.printTrace()
                #shares.cmd = None
                #run state 1 code
                if self.myuart.any() != 0:
                    #sanitize cmd
                    try:
                        self.check = True
                        self.cmd = int((self.myuart.readline()))
                        self.myuart.write('Updating Frequency of LED to ' + str(self.cmd) + '\r\n')
                        int(100/self.cmd)
                    except(ValueError, TypeError, ZeroDivisionError):                       
                        self.myuart.write('Please enter a valid non-zero integer')
                        self.cmd = None
                        shares5.cmd = None
                        self.check = False
                        
                    if self.check == True:    
                        self.transitionTo(self.S2_LED_PATTERN)                         
                    elif self.check == False:
                        pass
      
            elif(self.state == self.S2_LED_PATTERN):
                self.printTrace()
                #run state 2 code               
                shares5.cmd = self.cmd             
                self.transitionTo(self.S1_WAIT_FOR_INPUT)

            else:
                pass
        
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
                    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState          
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)         