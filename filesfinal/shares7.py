'''
@file shares7.py
@brief A container for all the inter-task variables
@author Mark Edin
'''

## Array containing the Reference speeds to be used by the controller task when updating the duty cycle. 
refspeed = None
## The Kp variable (gain) populated by the input received by TaskUser.
Kp = None
## The list that holds the speed data appended by the Controller task to be sent to the Front end by the TaskUser task.
timedata = [None] * 501
## The list that holds the time data appended by the Controller task to be sent to the Front end by the TaskUser task.
speeddata = [None] * 501
## The list that holds the position data appended by the Controller task to be sent to the Front end by the TaskUser task.
posdata =  [None] * 501

## A shared variable used to indicate when the desired reference velocity profile has been recreated by the motor. 
done = False




