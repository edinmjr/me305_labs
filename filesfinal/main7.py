"""
@file main7.py
@package main7
@brief A file used to intialize class objects and run tasks simultainiously. 
@details This main.py specifically constructed for lab 7 initializes the Encoder object (with the correct pins and timers), 
initializes the Motor object (with the correct pins and timer), and initializes the closed loop object with an intial value of 0.1,
it is changed later by the input given through the front end. 
@author Mark Edin
"""
from TaskUser7 import Task_usr   
from nucencoder import EncoderDriver
from MotorDriver import MotorDriver
from Controller7 import Controller
from ClosedLoopModule7 import ClosedLoop
import pyb
import sys


    
## PC6 Encoder pin object
PC6 = pyb.Pin.cpu.C6
## PC7 Encoder pin object
PC7 = pyb.Pin.cpu.C7
## Timer object used by the encoder, specifically used to host encoder waveform channels
tim = pyb.Timer(8, prescaler = 0, period = 0xffff)
#Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PC6, mode = pyb.Timer.ENC_AB)      
#Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PC7, mode = pyb.Timer.ENC_AB)
##EncoderDriver Obj that is passed into the Controller Task
Encoder = EncoderDriver(PC6, PC7, tim, 0xffff)


## Sleep Pin
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
## PB0 pin object passed into the motor driver object
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## PB1 pin object passed into the motor driver object
pinB1 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
pinB1.low()
## Timer object that is passed into the motor driver object
timer= pyb.Timer(3, freq = 20000)
#Motor driver object passed into the Controller Task
Motor = MotorDriver(nSLEEP_pin, pinB0, pinB1, timer)
## Closed loop object with initial Kp of 0.1
ClosedLoop = ClosedLoop(0.1)
## Interval for the controller task. It is specifically set to 30ms because the reference data used was sampled from the original ref data at every multiple of 30ms
interval = 30

## Task object for the Controller task
task0 = Controller(0, interval, Motor, Encoder, ClosedLoop, dbg=False)
## Task object for the Task_usr task
task1 = Task_usr(1, 50, dbg=False)
## Task list object for the task list being ran indefinitely
taskList = [task0,
            task1]
## A try/except case to completely kill the motors after a keyboard interrupt in REPL.
try:
    while True:
        for task in taskList:
            task.run()
except KeyboardInterrupt:
    print("Shutting Down")
    sys.exit()
    
