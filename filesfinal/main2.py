"""
@file main2.py
@package main2
@brief A main file to run both of lab 2's led tasks
@details This main file sets up a task list that runs lab 2's led tasks "simultaniusly"
@author Mark Edin
"""
from nucleoclasseslab2 import vLEDFSM
from nucleoclasseslab2 import vLED
from nucleoclasseslab2 import rLEDFSM
import pyb


    
## PA5 Pin object that is used to control the real LED
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## Interval between runs of the task in ms
interval = 500

## Task object for the virtual LED
task0 = vLEDFSM(interval, vLED)
## Task object for the real led
task1 = rLEDFSM(interval)
## Task list object containing both the vLED and rLED tasks that run indefinitely 
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()

    