# -*- coding: utf-8 -*-
"""
@file UI_FrontLab7.py
@package UI_FrontLab7
@author Mark Edin
"""

import matplotlib.pyplot as plt
import time
import keyboard
import serial
import math





class UI_Task:
    '''
    @class UI_Task
    @brief A UI task built in python designed to send commands to a nucleo microcontroller
    @details A UI built in python designed to send Kp to the TaskUser located on the nucleo.
    After sending the Kp value, it waits for 20 seconds for the desired velocity profile to be completed, and then receives the time, speed, and position
    data sent from the nucleo. It then plots that data over in conjunction with the original reference data in order to show how well the sent Kp value did. 
    The class also calculates the J value, which is a metric used to define the error present between the reference and measured velocity and position profiles (combined). 
    '''
    
    ## Initialization state
    S0_INIT = 0
    ## Wait for character ('g') state
    S1_WAIT_FOR_CHAR = 1
    ## Enter in data to be sent to the nucleo state
    S2_ENTER_DATA = 2
    ## Wait for the data to be generated and sent from the nucleo state
    S3_WAIT_FOR_DATA = 3
    ## Process the data received from the nucleo to generate step response plot state
    S4_PROCESS_DATA = 4
    
    def __init__(self, taskNum, interval, dbg=False):
        '''
        @brief This constructor runs when a UI_Task object is created
        @details The UI_Task is contructed with an intial task number, a run interval, and a debug boolean statement. 
        @param[in] The tasknum passed in through the main.py file running this task is what task # it is, the interval is a multiple of 50ms at which each run of the task 
        should be done, and dbg defines the dbg state. 
        '''
        ## The number of the task
        self.taskNum = taskNum
        ##  The amount of time in nanoseconds between runs of the task
        self.interval = int(interval*50000)
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## The timestamp for the first iteration
        self.start_time = time.perf_counter_ns()
        ## Counter that describes the number of times the task has run
        self.runs = 0
        ## The current time variable
        self.curr_time = 0
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        ## the difference in current time vs. next time
        self.diff_time = self.curr_time - self.next_time
        ## An array used to hold position data
        self.position = []
        ## An array used to hold time data
        self.time = []
        ## An array used to hold data read from the serial port that was sent from the nucleo
        self.readData = []
        ## Debugging
        if self.dbg:
            print('Created user interface task')
        ## The command variable that is set based off of what the user inputs
        self.g = None
        ## A variable that holds 1 line of data that was read from the serial port
        self.line_string = None
        ## The list of line_strings read from the serial port
        self.line_list = []
        ## A variable used to hold the true/false value of the input timeout loop
        self.x = 0
        ## Local variable to hold number of ref speed values to be sent to nucleo
        self.n = None
        ## Local Speed data array filled by the data sent from the nucleo
        self.speed = []
        ## Local time data array filled by the data sent from the nucleo
        self.time = []
        ## Local position data array filled by the data sent from the nucleo
        self.pos = []
        ## Time data str to be converted to float then int
        self.timestr = ''
        ## Speed data str to be converted to float then int
        self.vstr = ''
        ## Local variable used to store the position string read from the nucleo
        self.posstr = ''
        ## A local array holding the reference time values stored in the reference.csv
        self.reftime = []
        ## A local array holding the reference speed values stored in the reference.csv
        self.refspeed = []
        ## A local array holding the reference position values stored in the reference.csv
        self.refpos = []
        ## A local variable used to store the line read from the .csv file
        self.line = None
        ## The iteration index used to increment through the J term summation
        self.jidx = 0
        ## The local variable used to store the J term that was calculated by using the reisman summation method, (not including the 1/K term)
        self.J = 0
        ## The local variable of (1/K) * J that is printed to the console
        self.Jshow = 0
        ## The local variable used to store the serial object, this is used to communicate with the nucleo. This line initializes the serial port connection
        self.ser = serial.Serial(port = 'COM3', baudrate = 115200, timeout = 1)
        ## local variable used when taking the received time value from the ser.readline() loop
        self.t = 0
        ## local variable used when taking the received speed value from the ser.readline() loop
        self.v = 0
        ## local variable used when taking the received position value from the ser.readline() loop
        self.p = 0
        ## Local variable used to store the Kp value entered by the user
        self.usrKp = 0
        ## Local iteration variable used to store the iteration value used to control how many times the front end reads a line from the nucleo
        self.samidx = 0
        ## The local array used to store the collected time data that will be shown on the output graph depicting the velocity and position profiles
        self.plottime = []
        ## The local array used to store the collected speed data that will be shown on the output graph depicting the velocity and position profiles
        self.plotspeed = []
        ## The local array used to store the collected position data that will be shown on the output graph depicting the velocity and position profiles
        self.plotpos = []
        ## The local array used to store the reference time data that is used to plot the velocity and position profiles
        self.refplottime = []
        ## The local array used to store the reference speed data that is used to plot the velocity and position profiles
        self.refplotspeed = []
        ## The local array used to store the reference position data that is used to plot the velocity and position profiles
        self.refplotpos = []
        
    def run(self):
        '''
        @brief Runs one interation of the python UI task
        @details This run task is used in the maintestUI.py file to run the front end. I have decided to create a Task class with a run function instead of a stand
        alone script because it makes debugging a lot easier, plus it is what i am used to doing :) 
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = time.perf_counter_ns()
        if(self.start_time - self.interval >= 0):
            if(self.state == self.S0_INIT):
                self.printTrace()
                self.speed = []
                self.time = []
                self.readData = []
                self.g = None
                #Run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                           
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                #Run State 1 Code
                print("Press [g] to enter Kp and a reference velocity profile")
                self.a = time.time()
                self.x = True
                while self.x:
                    if ((keyboard.is_pressed('g')) or (time.time() > self.a + 1000)):
                        self.x = False
                        self.g = 'g'  
                if self.g == 'g':
                    self.transitionTo(self.S2_ENTER_DATA)
                    self.g = None
                                 
            elif(self.state == self.S2_ENTER_DATA):
                self.printTrace()
                #Run state 2 code
                self.usrKp = float(input("Please enter your desired Kp value: "))           
                self.ser.write('{:f}\r\n'.format(self.usrKp).encode('ascii'))
                self.transitionTo(self.S3_WAIT_FOR_DATA)
                
            elif(self.state == self.S3_WAIT_FOR_DATA):
                self.printTrace()
                #Run state 3 code, which is just waiting until the nucleo is done running 
                time.sleep(20)
                self.transitionTo(self.S4_PROCESS_DATA)      
                
            
            elif(self.state == self.S4_PROCESS_DATA):
                self.printTrace()
                #Run state 4 code
                self.ref = open('referencesbigfront.csv')
                #Reading the reference data that the nucleo also used to run its code
                while True:
                    self.line = self.ref.readline()
                    if self.line == '':
                        break
                    else:
                        (self.timestr,self.vstr,self.posstr) = self.line.strip('\n').split(',')
                        self.t = (float(self.timestr)*(1E3))
                        self.v = (float(self.vstr)*40)
                        self.p = (float(self.posstr)*40)                        
                        self.reftime.append(self.t)
                        self.refspeed.append(self.v)
                        self.refpos.append(self.p)
                self.ref.close()    
                #Reading data sent from nucleo and appending the values to their appropriate local lists
                while self.ser.in_waiting > 0 and self.samidx <= 500:
                    self.line_string = self.ser.readline().decode('ascii')
                    self.line_list = self.line_string.strip('\r\n').split(',')  
                    self.timestr = self.line_list[0]
                    self.speedstr = self.line_list[1]
                    self.posstr = self.line_list[2]
                    self.time.append(float(self.timestr))
                    self.speed.append(float(self.speedstr) * (math.pi/30)*40)
                    self.pos.append(float(self.posstr)*40)
                    self.samidx += 1
                    
                #Calculating J after gathering data from nucleo
                for self.jidx in range(501):
                    self.J += (((self.refspeed[self.jidx] - self.speed[self.jidx]) ** 2) + ((self.refpos[self.jidx]-self.pos[self.jidx]) ** 2))
 
                self.Jshow = self.J/(len(self.time))
                #Printing J 
                print('The performance metric J was calculated to be: ' + str((self.Jshow)))
                #Plot the data from self.time and self.speed
                plt.subplot(2,1,1)
                plt.plot(self.reftime, self.refspeed, label = 'Reference Velocity')
                plt.plot(self.time, self.speed, label = 'Real Velocity')
                #format axis
                plt.xlabel('Time (s)')
                plt.ylabel('Speed (RPM)')
                plt.title('Speed vs. Time')
                plt.legend()
                plt.grid(True)
                #Plot the data from self.time and self.pos
                plt.subplot(2,1,2)                
                plt.plot(self.reftime, self.refpos, label = 'Reference Position')
                plt.plot(self.time, self.pos, label = 'Real Position')
                plt.xlabel('Time (s)')
                plt.ylabel('Pos (deg)')
                plt.title('Position vs. Time')
                plt.grid(True)
                plt.legend()
                #Show plots
                plt.subplots_adjust(hspace=0.7)
                plt.show()
                self.transitionTo(self.S1_WAIT_FOR_CHAR)       
            else:
                pass 
            self.runs += 1           
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
           
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState
        
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
            
            
