"""
@file main4.py
@package main4
@brief A main.py file to run the datagen task.
@details This main.py holds the encoder setup code as well as the encoder driver object that is passed in to
the UI_dataGenTask. The task will run constantly, since it is in a Task like that is called within a "while True:"
loop. 
@author Mark Edin
"""
from UI_dataGen import UI_dataGenTask   
from nucencoder import EncoderDriver
import pyb


    
## PA6 encoder pin object passed into the encoder driver object
PA6 = pyb.Pin.cpu.A6
## PA7 encoder pin object passed into the encoder driver object
PA7 = pyb.Pin.cpu.A7
## Encoder timer object passed into the encoderdriver object
tim = pyb.Timer(3, prescaler = 0, period = 0xffff)
## Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PA6, mode = pyb.Timer.ENC_AB)      
## Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PA7, mode = pyb.Timer.ENC_AB)
## Encoder driver object passed into the UI_dataGenTask
Encoder = EncoderDriver(PA6, PA7, tim, 0xffff)
## Frequency of encoder driver updates passed into the UI_dataGenTask
interval = 5

## UI_dataGenTask passed into the task list
task0 = UI_dataGenTask(0, interval, Encoder, dbg=False)
## Task list that runs indefinitely on the nucleo
taskList = [task0]

while True:
    for task in taskList:
        task.run(interval)
