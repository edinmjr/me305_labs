'''
@file nucencoder.py
@package nucencoder
@author Mark Edin
'''
import utime
import pyb
import math

class EncoderDriver:
    '''
    @class EncoderDriver
    @brief A class that allows the encoder to be used in other classes/files.
    @details The EncoderDriver is contructed with pins PC6 and PC7, timer, and period passed in through the main.
    The encoder channels are also contructed in the main, but can be "uncommented" out if you would rather set the channels
    locally. 
    '''
    
	
    def __init__(self, PC6, PC7, tim, period):
        '''
        @brief This constructor runs when an EncoderDriver object is created
        @details The EncoderDriver is constructed with an initial PC6 pin, PC7 pin, timer, and period.
        @param[in] PC6 and PC7 describe the pins passed into that that the encoder uses to measure the tick differences
        @param[in] tim is the timer object passed into the class being used by both encoder channels
        @param[in] period is the period passed into the class of the timer used by the encoder
        '''
      
        ## Period of encoder channel 
        self.period = period
        ## Pin object 
        self.PB6 = PC6
        ## Pin Object
        self.PB7 = PC7
        ## Mode of Encoder
        self.mode = pyb.Timer.ENC_AB
        ## Count of the encoder ticks
        self.count = 0
        ## Previous tick count
        self.prev_count = 0
        ## Delta of encoder ticks
        self.delta = 0
        ## Good delta
        self.gdelta = 0
        ## Encoder Position
        self.position = 0
        #Position angle
        self.posangle = 0
        ## position in rad
        self.posrad = 0
        ## Position in revolution
        self.posrev = 0
        ## Encoder position
        self.m_position = 0
        ## Previous angle position
        self.angpos_prev = 0
        ## Most current angle position
        self.angpos_new = 0
        ## Timer object passed through main
        self.tim = tim
        ## Timestamp for speed (most current)
        self.timestamp_new = 0
        ## Timestamp for speed (last update)
        self.timestamp_prev = 0
        ## Converter for new encoder
        self.converter = 100/9
        ## Angluar speed 
        self.angspeed = 0
             
        #Channel 1 (A) timer
        #self.Ch1 = self.tim.channel(1, pin = self.PB6, mode = self.mode)
        
        #Channel 2 (B) timer
        #self.Ch2 = self.tim.channel(2, pin = self.PB7, mode = self.mode)
    
    def update(self):
        '''
        @brief Updates the encoders position. Checks for a bad delta and either subtracts or adds the period or period/2
        based on what the delta was. Does not return any number, but can be used before other EncoderDriver methods to 
        ensure that the encoder position/angle/speed is up to date. 
        '''
		
        self.prev_count = self.count
        self.count = self.tim.counter()
        
        if abs(self.count - self.prev_count) > self.period/2:
            
            self.delta = self.count - self.prev_count
            if self.delta > 0:
                self.gdelta = self.delta - self.period
                
            elif self.delta < 0:
                self.gdelta = self.delta + self.period
            
        elif abs(self.count - self.prev_count) < self.period/2:
            self.delta = self.count - self.prev_count
            self.gdelta = self.delta
            
        else:
            self.gdelta = 0
        
        self.m_position += self.gdelta
        
        self.angpos_prev = self.angpos_new
        self.angpos_new = int(self.m_position/self.converter)
        self.timestamp_prev = self.timestamp_new
        self.timestamp_new = utime.ticks_us()
        
        
    def getPosition(self):
        '''
        @return returns the motors current position
        @brief Returns current position of encoder
        '''
        return (self.m_position)
    
    def getAngle(self, units):
        '''
        @brief Get the angle position of the encoder, input type of units you want the angle to be returned as
        @param[in] depending on what you input (units), the function returns the angle of the encoder in different units
        @return The function returns the angle measurement of the encoder in degrees if 'units' is "deg", radians if the 'units' is "rad",
        and revolutions if the 'units' are "rev"
        '''
        self.posangle = self.m_position/self.converter
        self.posrad = self.posangle * (math.pi/180)
        self.posrev = self.posangle/360
        if (units == 'deg'):            
            return self.posangle
        if (units == 'rad'):
            return self.posrad
        if (units == 'rev'):
            return self.posrev
        
    def getSpeed(self, units):
        '''
        @brief Get the current speed of the encoder based on the last 2 updates. Input the type of units you want 
        the speed to be returned as
        @param[in] depending on what you input (units), the function returns the angular velocity of the encoder in different units
        @return The function returns the angular velocity measurement of the encoder between the 2 most recent updates 
        in degrees/s if 'units' is "deg/s", rad/s if the 'units' is "rad/s", and revolutions/s if the 'units' are "rev/s"
        '''
        if (units == 'deg/s'):
            self.angspeed = (10**6)*((self.angposnew - self.angpos_prev)/utime.ticks_diff(self.timestamp_new, self.timestamp_prev))
            return self.angspeed
        elif (units == 'rad/s'):
            self.angspeed = (10**6)*(math.pi/180)*((self.angpos_new - self.angpos_prev)/utime.ticks_diff(self.timestamp_new, self.timestamp_prev))
            return self.angspeed
        elif (units == 'RPM'):
            self.angspeed = (10**6)*(60/360)*((self.angpos_new - self.angpos_prev)/utime.ticks_diff(self.timestamp_new, self.timestamp_prev))
            return self.angspeed
        

    def setPosition(self, position):
        '''
        @brief Sets the current encoder to position to the input parameter "position"
        @param[in] The position input is what the motor position will be set to 
        '''
        self.m_position = position
        
    def getDelta(self):
        '''
        @brief Returns the difference in recorded position between the two most recent calls to update()
        @return This function returns the current delta between the 2 most recent encoder updates
        '''
        return self.delta
    

    def zero(self):
        '''
        @brief Completely zeros out the encoder position and sets it back to zero
        @return This function sets the motor position to "0" and returns the motor position (which will always be zero)
        '''
        self.m_position = 0
        return (self.m_position)
        
        
        
        
class EncoderTask:
    '''
    @class Encoder Task
    @brief A task that runs and updates the encoder position and outputs it 
    @details A task that is constantly updating the position of the encoder. It can be used in conjunction with other 
    tasks that constantly need the updated encoder position. 
    '''
    ## Initialization State
    S0_INIT = 0
    ## RUnning state
    S1_RUNNING = 1
    
    
    def __init__(self, interval, EncoderDriver):
        '''
        @brief This function initializes the encoder task object that was used in lab 3. 
        @details This function initializes the encoder task object that was used in lab 3 to constantly update the encoder and print out the encoder position
        to the terminal. 
        @param[in] The interval input is the amount of time between runs of the task 
        @param[in] The EncoderDriver object input is a class coby of the encoder driver task used to update the encoders position in the task.
        '''
        ## Class copy of the EncoderDriver object
        self.EncoderDriver = EncoderDriver
        ## Initial state
        self.state = self.S0_INIT
        ## Start time of the task
        self.start_time = utime.ticks_us()
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval)
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## Runs of the task
        self.runs = 0
        #Current time variable used
        self.curr_time = 0
       
    def run(self):
        '''
        @brief Runs one iteration of the EncoderTask Task. Waits until the interval has been reached to run again. 
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:   
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_RUNNING)   
                
            elif (self.state == self.S1_RUNNING):       
                    self.EncoderDriver.update()
                    self.EncoderDriver.getPosition()
            else:
                pass
            self.runs += 1
            self.next_time += self.interval
        
    def transitionTo(self, NewState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = NewState
    
