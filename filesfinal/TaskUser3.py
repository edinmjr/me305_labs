'''
@file TaskUser3.py
@package TaskUser3
@brief User interface task as a finite state machine
@author Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''


import shares3
import utime
from pyb import UART

class TaskUser1:
    '''
    @class TaskUser1
    User interface task.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, taskNum, interval, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1E2)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        ## Current time timestamp
        self.curr_time = 0
        
        
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
         
            
        if(self.state == self.S0_INIT):
            self.printTrace()
            # Run State 0 Code
            self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
        elif(self.state == self.S1_WAIT_FOR_CHAR and utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            print('Please enter a command:  ')
            self.printTrace()
            self.next_time += self.interval
             # Run State 1 Code
            if self.ser.any():
                shares3.cmd = self.ser.readchar()
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                self.transitionTo(self.S2_WAIT_FOR_RESP)
                    
        elif(self.state == self.S2_WAIT_FOR_RESP and utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            self.printTrace()
           # Run State 2 Code
            if(shares3.cmd == 112):
                shares3.cmd = None               
                print(shares3.resppos)
                self.next_time += self.interval*100          
                self.next_time += self.interval
            elif(shares3.cmd == 122):
                shares3.cmd = None       
                print(shares3.resppos) 
                self.next_time += self.interval*100
            elif(shares3.cmd == 100):
                shares3.cmd = None     
                print(shares3.resdelt)
                self.next_time += self.interval*100      
        else:

               pass
            


    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)