# -*- coding: utf-8 -*-
"""
@file main3.py
@package main3
@brief main.py file for lab 3
@author: edinm
"""


from nucencoder import EncoderTask, EncoderDriver
from TaskUser import TaskUser1
import pyb

## PB6 encoder pin object
PB6 = pyb.Pin.cpu.B6
## PB7 encoder pin object
PB7 = pyb.Pin.cpu.B7
## Timer object used to host the encoder channels
tim = pyb.Timer(4, prescaler = 0, period = 0xffff)
## Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PB6, mode = pyb.Timer.ENC_AB)      
## Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PB7, mode = pyb.Timer.ENC_AB)
## Encoder driver object that is passed into the encoder task
Encoder = EncoderDriver(PB6, PB7, tim, 0xffff)


## Encoder Task object
task0 = EncoderTask(1000, Encoder)
## Task User task object 
task1 = TaskUser1(1, 10000, dbg = True)

## Task list object that runs task0/1 indefinitely on the nucleo
tasklist = [task0,
            task1]

while True:
    for task in tasklist:
        task.run()