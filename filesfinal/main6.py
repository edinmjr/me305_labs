"""
@file main6.py
@brief A file used to intialize class objects and run tasks simultainiously. 
@details This main.py initializes the Encoder object (with the correct pins and timers), initializes the 
Motor object (with the correct pins and timer), and initializes the closed loop object with an intial value of 0.1,
it is changed later by the input given through the front end. 
@author Mark Edin
"""
from TaskUser6 import Task_usr   
from nucencoder import EncoderDriver
from MotorDriver import MotorDriver
from Controller6 import Controller
from ClosedLoopModule import ClosedLoop
import pyb
import sys


    
## PC6 encoder pin object
PC6 = pyb.Pin.cpu.C6
## PC7 encoder pin object
PC7 = pyb.Pin.cpu.C7
## Timer object used by the encoder channels in lab 6
tim = pyb.Timer(8, prescaler = 0, period = 0xffff)
#Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PC6, mode = pyb.Timer.ENC_AB)      
#Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PC7, mode = pyb.Timer.ENC_AB)
## Encoder driver object that is passed into the Controller task
Encoder = EncoderDriver(PC6, PC7, tim, 0xffff)


#Motor Sleep Pin object
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
## PB0 motor pin object passed into the motor driver
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
## PB1 motor pin object passed into the motor driver
pinB1 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
pinB1.low()
## Timer object passed into the motor driver object
timer= pyb.Timer(3, freq = 20000)
## Motor driver object that is passed into the Controller task
Motor = MotorDriver(nSLEEP_pin, pinB0, pinB1, timer)
## Closed loop object that is passed into the Controller task
ClosedLoop = ClosedLoop(0.1)
## Interval of both the controller and Task_usr tasks
interval = 30

## Controller Task object
task0 = Controller(0, interval, Motor, Encoder, ClosedLoop, dbg=False)
## Task_usr object
task1 = Task_usr(1, interval, dbg=False)
## Task list object that runs indefinitely on the nucleo
taskList = [task0,
            task1]
## A try/except case to completely kill the motors after a keyboard interrupt in REPL.
try:
    while True:
        for task in taskList:
            task.run()
except KeyboardInterrupt:
    print("Shutting Down")
    sys.exit()
    
