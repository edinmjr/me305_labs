# -*- coding: utf-8 -*-
"""
@file main5.py
@brief A main.py file to run on the nucleo holding the BLE and LED controller tasks
@details This main.py file creates 2 objects, and sets them to individual tasks. Each task runs seperate from eachother
and allows the LED controller to run while the BLEModuleDriver task is running. 
@author Mark Edin
"""

from LEDController import LEDController 
from Lab5classes import BLEModuleDriver

## Interval that the tasks will run at. i.e. the amount of time in ms between task runs
interval = 800

## Bluetooth module driver task
task0 = BLEModuleDriver(0, interval, dbg=True)
## LED Controller driver task
task1 = LEDController(1, interval, dbg=True)
## Task List containing both tasks that will run indefinitely on the nucleo
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()
        #task.run()
        
    
