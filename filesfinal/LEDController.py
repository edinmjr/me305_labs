# -*- coding: utf-8 -*-
"""
@file LEDController.py
@package LEDController
@author: edinm
"""

import utime
import pyb
import shares5


class LEDController:
    '''
    @class LEDController
    @brief A class used to control the real nucleo led
    @details This class represents an FSM that allows the LED on the nucleo to change its brightness to represent a saw wave. 
    '''
    ## Init state
    S0_INIT = 0
    ## LED on state
    S1_LED_ON = 1
    ## LEd off state
    S2_LED_OFF = 2
    
    
    def __init__(self, taskNum, interval, dbg=True):
        '''
        @brief This constructor runs when a LEDController object is created
        @details This constructer initializes all of the local variables needed to run the LEDController task. 
        @param[in] The tasknum passed in through the main.py file running this task is what task # it is, the interval is a multiple of 50ms at which each run of the task 
        should be done, and dbg defines the dbg state. 
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT       
        ## The name of the task
        self.taskNum = taskNum        
        ## A counter showing the number of times the task has run
        self.runs = 0        
        ## The interval of time, in freq, between runs of the task
        self.interval = int(interval)        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()              
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        ## local command variable
        self.cmd = None
        ## Current time stamp
        self.curr_time = 0
        ## Pin object used to control the nucleo's led
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        ## Debugging
        if self.dbg:
            print('Created scaler task: LEDController') 
        
    def run(self):
        '''
        @brief runs one iteration of the task
        @details This run task is used to change the brigtness of the LED located on the nucleo. The task updates the frequency of the LED from a value that is 
        set in the shares
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):
                self.printTrace()
                #run state 0 code
                self.transitionTo(self.S1_LED_ON)
                
                
            elif(self.state == self.S1_LED_ON): 
                self.printTrace()
                #run state 1 code
                self.updateFreq(shares5.cmd)
                self.pinA5.high()
                self.transitionTo(self.S2_LED_OFF)
                    
            elif(self.state == self.S2_LED_OFF): 
                self.printTrace()
                #run state 2 code
                self.updateFreq(shares5.cmd)
                self.pinA5.low()
                self.transitionTo(self.S1_LED_ON)
                                      
            else:
                pass
            
            self.runs += 1
             # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
        
        
    def updateFreq(self, freq):
        '''
        @brief Updates the blink frequency of the LED, it will not update if there is no new frequency
        @param[in] The freq input for this method sets the shares5.cmd value, which in turn updates the frequency of the LED blink on the nulceo
        '''
        if shares5.cmd != None:
            self.interval = int(1000/freq)
            shares5.cmd = None
        else:
            pass    
        
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState          
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)         