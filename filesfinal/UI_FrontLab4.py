# -*- coding: utf-8 -*-
"""
@file UI_FrontLab4.py
@package UI_FrontLab4
@author Mark Edin
"""

import matplotlib.pyplot as plt
import time
import serial
import keyboard
import numpy as np

class UI_Task:
    '''
    @class UI_Task
    @brief A UI built in python designed to send commands to a nucleo microcontroller
    @details A UI built in python designed to send commands to a nucleo microcontroller connected to a rotary encoder,
    and also recieves rotary encoder data after 10s or until the 's' key is pressed. The class is written as a task,
    so it should be ran in a main.py file locally through a task list. Once data is recieved from the nucleo, a graph of encoder position vs. 
    time is constructed, and a .csv file is saved to the local working folder (same folder this file is saved in). 
    '''
    
    ## Init state
    S0_INIT = 0
    ## Wait for character press state   
    S1_WAIT_FOR_CHAR = 1
    ## Wait for response state
    S2_WAIT_FOR_RESPONSE = 2
    ## Process incoming data state
    S3_PROCESS_DATA = 3
    
    def __init__(self, taskNum, interval, dbg=False):
        '''
        @brief
        @details
        '''
        ## THe task num, used for dbg
        self.taskNum = taskNum
        ##  The amount of time in nanoseconds between runs of the task
        self.interval = int(interval*1E9)
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        ## The timestamp for the first iteration
        self.start_time = time.perf_counter_ns()
        ## Counter that describes the number of times the task has run
        self.runs = 0
        ## The current time local variable
        self.curr_time = 0
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        ## the difference in current time vs. next time
        self.diff_time = self.curr_time - self.next_time
        ##Serial Port initializatoin
        self.ser = serial.Serial(port = 'COM3', baudrate = 115273, timeout = 1)
        ## An array used to hold position data
        self.position = []
        ## An array used to hold time data
        self.time = []
        ## An array used to hold data read from the serial port that was sent from the nucleo
        self.readData = []
        ## Debugging
        if self.dbg:
            print('Created user interface task')
        ## The command variable that is set based off of what the user inputs
        self.cmd = None
        ## A variable that holds 1 line of data that was read from the serial port
        self.line_string = None
        ## The list of line_strings read from the serial port
        self.line_list = []
        ## A variable used to hold the true/false value of the input timeout loop
        self.x = 0
              
    def run(self):
        '''
        @brief Runs one interation of the python UI task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = time.perf_counter_ns()
        if(self.start_time - self.interval >= 0):
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                self.position = []
                self.time = []
                self.readData = []
                self.cmd = None
                #Run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                #Run State 1 Code
                print("[g] Begin data collection")
                self.a = time.time()
                self.x = True
                while self.x:
                    if ((keyboard.is_pressed('g')) or (time.time() > self.a + 1000)):
                        self.x = False
                        self.cmd = 'g'  
                if self.cmd == 'g':
                    self.ser.write(str(self.cmd).encode('ascii'))
                    self.cmd = None
                    self.t0 = time.time()
                    self.transitionTo(self.S2_WAIT_FOR_RESPONSE)
                    
            elif(self.state == self.S2_WAIT_FOR_RESPONSE):
                self.printTrace()
                #Run state 2 code         
                print("[s] Stop Data Collection") 
                self.a = time.time()
                self.x = True
                while self.x:
                    if ((keyboard.is_pressed('s')) or (time.time() > self.a + 10)):
                        self.x = False
                        self.cmd = 's'               
                if self.cmd == 's':
                    print('Ending Data Collection')
                    self.ser.write(str(self.cmd).encode('ascii'))
                    self.cmd = None
                    time.sleep(1)
                    #print('Responses in waiting: ' + str(self.ser.in_waiting))                    
                    while self.ser.in_waiting > 0:
                        self.line_string = self.ser.readline().decode('ascii')
                        self.line_list = self.line_string.strip('\r\n').split(',')
                        self.position.append(int(self.line_list[1]))
                        self.time.append(int(self.line_list[0]))   
                    self.transitionTo(self.S3_PROCESS_DATA)      
            elif(self.state == self.S3_PROCESS_DATA):
                #self.curr_time = time.perf_counter_ns()
                self.printTrace()
                #Run State 3 code    
                #Plot the data from self.time and self.position
                plt.plot(self.time, self.position)
                #format axis
                plt.xlabel('Time (us)')
                plt.ylabel('Encoder Position (Degrees)')
                plt.title('Encoder Position vs. Time')
                plt.grid(True)
                #output graph
                plt.show()
                #output data to .csv file
                np.savetxt('dataVals.csv', (self.time, self.position), delimiter=',')
                
                self.transitionTo(self.S0_INIT)
                    
                
            else:
                pass
            
            self.runs += 1           
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
        
              
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState
        
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.diff_time)
            print(str)
            
            
