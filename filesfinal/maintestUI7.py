'''
@file maintestUI7.py
@package maintestUI7
@brief Used to run the UI front for Lab7. 
@details Tasks imported and present in the task list are ran continuously. 
'''

from UI_FrontLab7 import UI_Task   

## Task object
task0 = UI_Task(0, .5, dbg=True)
## Task list holding tasksg
taskList = [task0]

while True:
    for task in taskList:
        task.run()
