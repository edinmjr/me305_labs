# -*- coding: utf-8 -*-
"""
@file TaskUser7.py
@package TaskUser7
@author Mark Edin
"""


import utime
from pyb import UART
import shares7


class Task_usr:
    '''
    @Class Task_usr
    @brief A task class that runs on the nucleo in conjunction with the Controller task. 
    @details This class allows the nucleo to recieve parameters such as Kp and reference speeds from the Front end running on the computer through 
    the serial port. While running, the task waits for the values sent through UART and assigns those values to shared variables located in shares.py. 
    Once the task has shared the input values, it waits until a "Done" command is read from shares.done (set by the Controller Task) and then sends the time, speed, 
    and position data back to the computer through the serial port. 
    '''  
    ## Initialization state
    S0_INIT             = 0  
    ## Wait for input variables State
    S1_WAIT_FOR_USR_INPT     = 1 
    ## update Data State
    S2_UPDATE_DATA  = 2

    def __init__(self, taskNum, interval, dbg=False):
        '''
        @brief This constructor runs when a Task_usr object is created
        @details The Task_usr is constructed with an intial task numbner, interval, and debug boolean value.
        '''
        ## The number of the task
        self.taskNum = taskNum 
        ##  The sample frequency of the encoder
        self.interval = int(interval)              
        ## Flag to print debug messages or supress them
        self.dbg = dbg    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()   
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)    
        ## Class copy of the UART object used to communicate with the UI through the serial port 
        self.usb = UART(2)          
        ## Debugging
        if self.dbg:
            print('Created scaler task')   
        ## A variable to hold the most recent line read from the serial port
        self.linestring = None
        ## A list used to hold the input variables sent from the Front end on the computer
        self.linelist =[]
        ## A local list used to hold the reference variable given by the front end running on the PC
        self.refspeed = [None] * 501
        ## Local variable to store the Kp value sent by the front end
        self.Kp = 0
        ## Local variable to store the counter that is used to read the reference speed data stored locally.    
        self.refidx = 0       
        ## A local variable used to store the time data that is collected by the Controller task
        self.timedata = []
        ## A local variable used to store the speed data that is collected by the Controller task
        self.speeddata = []
        ## A local variable used to store the position data that is collected by the Controller task
        self.posdata = []
        ## A local variable used to store the Kp string read from the front end through the serial port
        self.Kpstr = 0
        ## A local variable used to store the .csv file object that is read (to create reference speed list)
        self.ref = None
        ## A local variable used to store the current readline() output from the .csv file
        self.vstr = ''
        
    def run(self):
        '''
        @brief Runs one iteration of the TaskUser Task. Waits until the interval has been reached to run again. 
        @details This run task is used to control the data transfer between the nucleo and the front end running on the PC. The run task class waits for an input (of Kp) from
        the front end (sent through serial), and then proceeds to set shares.Kp to Kp. The local reference speed values are read from the .csv file line by line and are incremented
        and added to a premade list containing 501 ref speed values. Once the task has set the shared Kp and ref speed values, it waits for a "done" command set by the controller to 
        send the data back. Once it receives that command, it sends the time, speed, and position data back to the front end in the format of (t,v,x). 
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:   
            if(self.state == self.S0_INIT):
                self.printTrace()                
                #run state 0 code
                if self.usb.any() != 0:
                    self.transitionTo(self.S1_WAIT_FOR_USR_INPT)   
                
            elif(self.state == self.S1_WAIT_FOR_USR_INPT):
                self.printTrace()
                #Run state 1 code
                shares7.done = False
                #Reading Kp value sent from the front end
                self.linestring = self.usb.readline().decode('ascii')                        
                self.Kpstr = (self.linestring)
                self.Kp = float(self.Kpstr)  
                ## Reading the ref speed data stored locally on the nucleo
                self.ref = open('referencesbig.csv')
                while True:
                    self.line = self.ref.readline()
                    if self.line == '':
                        break
                    else:
                        self.vstr = self.line.strip('\n')#.split(',')
                        self.v = (int(float(self.vstr)))
                        self.refspeed[self.refidx] = self.v
                        #self.refposition.append(float(x))
                        self.refidx += 1
                self.ref.close()   
                shares7.refspeed = self.refspeed
                shares7.Kp = self.Kp
                self.refidx = 0
                self.transitionTo(self.S2_UPDATE_DATA)   
                     
            elif(self.state == self.S2_UPDATE_DATA):
                self.printTrace()         
                #state 2 data here:                                                   
                if shares7.done == True:
                    self.updateData(shares7.timedata, shares7.speeddata, shares7.posdata)
                    for self.n in range(len(self.speeddata)):
                        print('{:},{:},{:}'.format(self.timedata[self.n], self.speeddata[self.n], self.posdata[self.n]))
                    self.transitionTo(self.S1_WAIT_FOR_USR_INPT)  
                    
            else:
                pass
            # Adding a run to the dbg run counter
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
            
           
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
           
          
    def updateData(self, timedata, speeddata, posdata):    
        '''
        @brief Updates the local time and speed data lists with the completed time and speed data lists located in shares.py
        @param[in] The inputs to this function should be lists containing time, speed, and position data. The function then sets the inputs
        (which can be a shared file that was updated by the controller task) to task local lists that can then be sent back to the front end 
        over the serial port. 
        '''
        self.timedata = timedata
        self.speeddata = speeddata
        self.posdata = posdata
        
        
    
        
        
        
        