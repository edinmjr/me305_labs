'''
@file nucleoclasseslab2.py
@package nucleoclasseslab2
@author Mark Edin
'''


import pyb, utime
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 2000000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin = pinA5)

class vLEDFSM:
    '''
    @class vLEDFSM
    @brief A FSM that allows me to change LED brightlness for the nucleo board.
    @details A finite state machine that allows me to change physical LED brightness according to one of the following patterns provided in the lab handout
    ''' 
    ## Constant defining State 0 - Initialization
    S0_INIT = 0   
    ## Constant defining State 1 - LED off
    S1_LED_OFF = 1  
     ## Constant defining State 2 - LED on
    S2_LED_ON = 2
 
    def __init__(self, interval, vLED):
        '''
        @brief This constructor runs when a Virtual LEDFSM object is created
        @details The vLEDFSM is constructed with an initial interval, and vLED object. 
        '''  
        ## State 0, LED initialization
        self.state = self.S0_INIT
        #A class attribute "copy" of the virtual LED object
        self.vLED = vLED 
        #A class attribute "copy" of the real LED object
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
          ## The interval of time, in milliseconds, between runs of the task
        self.interval = int(interval)
         ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.curr_time = 0
        
        
    
    def run(self):
        '''
        @brief Runs one interation of the task
        @details The virtual LED will "blink" (print) its state, whether that is its on or off at an interval 
        passed through the main.py file. 
        ''' 
       
        self.curr_time = utime.ticks_ms() #updating the current timestamp     
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:          
            if(self.state == self.S0_INIT):
                #print(str(self.runs) + ' State 0 {:0.2f}'.format(self.diff_time))
                #run state 0 code
                self.transitionTo(self.S1_LED_OFF)
                
            elif(self.state == self.S1_LED_OFF):
                #print(str(self.runs) + ' State 1 {:0.2f}'.format(self.diff_time))
                #Run state 1 code
                self.vLED.vOff()
                self.transitionTo(self.S2_LED_ON)
                
                
            elif(self.state == self.S2_LED_ON):
                #print(str(self.runs) + ' State 2 {:0.2f}'.format(self.diff_time))
                #Run STate 2 Code  
                self.vLED.vOn()
                self.transitionTo(self.S1_LED_OFF)
               
            
            
            else:
                pass
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
   
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
            
class vLED:
    '''
    @class vLED
    @brief      A virtual LED.
    @details    This class represents a virtual LED that can be turned on and off. 
    '''
    
    def __init__(self):
        '''
        @brief Creates a virtual LED object
        '''
        pass
    
    def vOn(self):
        '''
        @brief turns virtual LED on 
        @return Returns False is function is called
        '''
        print('Virtual LED On')
        return True
    
    def vOff(self):
        '''
        @brief turns virtual LED off
        @return Returns true if function is called
        '''
        print('Virtual LED Off')
        return True
    

class rLEDFSM:
    '''
    @class rLEDFSM
    @brief An fsm for the Real lEd on the nucleo
    @details a class encapulating the ability to control real LED on the nucleo. It has one main method named run that is called in the real led FSM task in lab2's 
    main2.py file. 
    '''
    ## Init state
    S0_INIT = 0 
    ## S1 brightness of real LED changing
    S1_LED_BRIGHTNESS_CHANGE = 1
    
    def __init__(self, interval):
         '''
         @brief This constructor runs when an rLEDFSM object is created
         @details This rLEDFSM is constructed with an intial interval, that is passed on to the 
         rLEDFSM object through the task object in the main.py file. 
         '''
         ## Init State 
         self.state = self.S0_INIT
         ## NUmber of runs of the task
         self.runs = 0
         ## The timestamp for the first iteration
         self.start_time = utime.ticks_ms()
         ## The interval of time, in seconds, between runs of the task
         self.interval = int(interval)
         ## The "timestamp" for when to run the task next
         self.next_time = utime.ticks_add(self.start_time, self.interval)
         ## The current timestamp variable
         self.curr_time = 0
         ## The iteration variable used to cycle through the saw wave pattern
         self.idx = 0
         
    def run(self):
        '''
         @brief Runs one iteration of the real LED FSM task
         @details The real LED will increase its brightness from 0 to 1% until it reaches 100% brightness, 
         and then it will return to 0 and start the process over again. The LED pattern can be defined as a saw
         wave function. 
         '''
        self.curr_time = utime.ticks_ms() #updating the current timestamp
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if(self.state == self.S0_INIT):   
                self.transitionTo(self.S1_LED_BRIGHTNESS_CHANGE)
                
            elif(self.state == self.S1_LED_BRIGHTNESS_CHANGE):         
                if self.idx <= (100):
                    t2ch1.pulse_width_percent(self.idx)
                    self.idx += 1
      
                else:
                    t2ch1.pulse_width_percent(0)
                    print('setting to 0')
                    self.runs+= 1
                    self.idx = 0
        
            else:
                pass
                
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
    
  
    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState        
            

        
      
            
            
            
     