# -*- coding: utf-8 -*-
"""
@file Controller7.py
@package Controller7
@author: Mark Edin
"""


import utime
import shares7


class Controller:
    """
    @class Controller
    @brief A controller module used to handle the updates of various sensors and set the duty cycle of the motor.
    @details This controller class is meant to be ran as a task in a main.py file located on the nucleo. The controller
    has the MotorDriver, EncoderDriver, and ClosedLoop classes all passed in (through the main.py) so that the controller
    task can make use of them. The class takes a value of Kp and an initial reference velocity, and sets the duty cycle
    to whatever is calculated in the ClosedLoop.update() function in order to reach the desired motor speed. Once the 
    desired speed has been set, shares.done is set to true which triggers a response from the TaskUser.py task. 
    """
    ## Initialization State
    S0_INIT = 0
    ## Check Duty Cycle State
    S1_RUN = 1
   
    
    
    def __init__(self, taskNum, interval, MotorDriver, EncoderDriver, ClosedLoop, dbg=False):
        '''
        @brief This constructor runs when a Controller object is created
        @details The Controller is contructed with an initial task number, interval, MotorDriver, EncoderDriver
        , ClosedLoop, and debug value that are passed in through the main.py that this task is running in. 
        '''
        ## A class copy of the MotorDriver object
        self.MotorDriver = MotorDriver
        ## A class copy of the EncoderDriver object
        self.EncoderDriver = EncoderDriver
        ## A class copy of the ClosedLoop object
        self.ClosedLoop = ClosedLoop
        ## Enabling the motor using the MotorDriver object
        self.MotorDriver.enable() 
        ## The name of the task, used for debugging
        self.taskNum = taskNum 
        ##  The amount of milliseconds between each run of the task. 
        self.interval = int(interval)              
        ## Flag to print debug messages or supress them
        self.dbg = dbg    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()   
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)    
        ## Creates a debug object if 'dbg = True' in the main. 
        if self.dbg:
            print('Created scaler task')
        ## Updating the encoder driver in order to perform a speed calculation on the first run of the task. 
        self.EncoderDriver.update()
        ## A local variable assigned to hold the "real" speed of the motor
        self.Oreal = 0
        ## A local variable object to hold the reference velocity that the task attempts to reach.
        self.Oref = 0
        ## A variable to hold the most current sample of L based on the difference between Oref and Oreal
        self.dutynew = 0
        ## The previous duty cycle calculated by adding dutycurr and duty new (previous run)
        self.dutyold = 0
        ## The duty cycle that the motor will be set to, it is calculated by adding the old duty cycle plus the newly calculated one
        self.dutycurr = 0 
        ## A variable to store the timestamp of a specific speed sample
        self.Otime = 0
        ## A local variable used to store the most recent update of the speed
        self.posdata = 0
        ## A local variable used to store the reference speed iteration variable. 
        self.refidx = 0
        ## The time when data collection begins, used to calculate the sample time
        self.start_time = 0
        ## THe local variable used to store the time when data was collected and then is added to the data list that will be sent back to the nucleo. 
        self.sample_time = 0
        
        
    def run(self):
        '''
        @brief Runs one iteration of the Controller Task. Waits until the interval has been reached to run again. 
        @details This run task is used in conjunction with the Task_usr task in order to set the motor to track the reference profile data that
        is stored locally. The task waits for Kp to be set (which only happens when the Task_usr has received Kp) and proceeds to update the current
        timesteps target speed, set the duty to that speed, update the speed and timestamp, and then add all of that data to 3 spereate appropriate shared lists.
        The task knows when to finish data collection due to the if statement that checks if the iteration index is less than the length of the reference speed array -1.
        '''
        
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0: 
            if(self.state == self.S0_INIT):
                self.printTrace()            
                if shares7.Kp != None:
                    self.transitionTo(self.S1_RUN)
                    self.ClosedLoop.set_Kp(shares7.Kp)
                    self.start_time = utime.ticks_ms()
            elif(self.state == self.S1_RUN):
                self.printTrace()
                if self.refidx == len(shares7.refspeed) -1:
                    shares7.done = True
                    self.MotorDriver.set_duty(0)
                    self.MotorDriver.disable()
                    shares7.Kp = None
                    self.transitionTo(self.S0_INIT)
                    
                self.Oref = shares7.refspeed[self.refidx]
                self.dutynew = self.ClosedLoop.update(self.Oref, self.Oreal)
                self.dutycurr = self.dutyold + self.dutynew
                self.dutyold = self.dutycurr   
                self.MotorDriver.set_duty(self.dutycurr)            
                self.EncoderDriver.update()
                self.Otime = utime.ticks_ms()
                self.sample_time = utime.ticks_diff(self.Otime, self.start_time)
                self.Oreal = self.EncoderDriver.getSpeed('rad/s')
                self.posdata = self.EncoderDriver.getAngle('rad')
                self.addData(self.sample_time, self.Oreal, self.posdata, self.refidx)
                self.refidx +=1
               
            
        
            else:
                pass
            
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
            
    def addData(self, timedata, speeddata, posdata,refidx):
        '''
        @brief Appends the most current sample of time, speed, and position data to an array (shares.realdata) that TaskUser.py uses to send back to the front end. 
        @param[in] The timedata, speeddata, posdata inputs are used to set the appropriate shared list values to the data that was collected at each sample point. The refidx
        input is used to iterate through the previously constructed shared list so that the appropriate t, s, and x values are recorded and stored in order. i would use append
        to do this but sadly our nucleo does not have enough memory to do so. It's a tragedy that numpy isn't included in pyb ):
        '''
        
        shares7.timedata[refidx] = timedata
        shares7.speeddata[refidx] = speeddata
        shares7.posdata[refidx] = posdata
   