"""
@file MotorDriver.py
@package MotorDriver
@author Mark Edin
"""

import pyb

class MotorDriver:
    '''
    @class MotorDriver
    @brief A module used to control the motors on our breakout board.
    @details This class allows us to control the speed and direction of the motors on our breakout board. The class has functions to enable, disable, 
    and set the duty cycle of the motors based on the pulse width percentage applied to the + or - pins. 
    '''
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''
        @brief This constructor runs when a MotorDriver object is created
        @details This MotorDriver takes in passed variables for the Sleep pin, IN1 pin, IN2 pin, and the timer associated
        with the specfic motor being used. 
        @param[in] The nSleep_pin passed through correlates to the sleep pin of the physical motor, The IN1 and IN2 pins are the "positive and negative" leads
        of the motor that voltage is applied to. The timer object passed in is the timer used by the 2 channels to control the pulses sent to the pins (IN1 and IN2)
        in order to drive the DC motor. 
        '''
        ## Sleep Pin object
        self.sleep_pin = nSLEEP_pin
        # Setting the sleep pin low before even thinking about setting it high. It makes a VERY loud noise if you use .enable() before doing this step. 
        self.sleep_pin.low()
        ## A class copy of the IN1 pin object
        self.IN1_pin = IN1_pin
        ## A class copy of the IN2 pin object
        self.IN2_pin = IN2_pin
        ## A class copy of the Timer object
        self.timer = timer
        ## class copy of the channel 3 timer object. The reason its using channel 3 rather than channel 1 is because my motor quality was subpar and unable to spin,
        # even while applying large amounts of force.
        self.t3ch1 = self.timer.channel(3, pyb.Timer.PWM, pin = IN1_pin)
        ## class copy of the Channel 4 timer Object, ditto with above. 
        self.t3ch2 = self.timer.channel(4, pyb.Timer.PWM, pin = IN2_pin)
    
    def enable(self):
        '''
        @brief This method enables the motor by setting the sleep pin to high. 
        '''
        self.sleep_pin.high()
        #print('Enabling Motor')
        
        
    def disable(self):
        '''
        @brief This method disables the motor by setting the sleep pin to low. 
        '''
        self.sleep_pin.low()
        
    def set_duty(self, duty): 
        '''
        @brief A function that sets the duty cycle of the motor (a percentage of 100% effort)
        @details Sets the duty cycle of the motor to whatever was given to the input parameter "duty". There is a 
        failsafe that makes sure there is never more than 100% effort requested from the motor, even if the requested effort
        is negative or positive. Also, if the duty cycle requested is larger than +- 100%, it sets the duty cycle to 100%. 
        @param[in] The duty input parameter is the duty cycle that you want the motor to run at. 100 is max, -100 is minimum. Positive values
        produce a CCW motion while negative values produce a CW motion.
        '''      
        self.duty = duty
        if self.duty >= 0 and self.duty <= 100:
            self.t3ch1.pulse_width_percent(self.duty)
            self.IN2_pin.low()
        elif self.duty > 100:
            self.t3ch1.pulse_width_percent(100)
            self.IN2_pin.low()
        elif self.duty < 0 and self.duty >= -100:
            self.IN1_pin.low()
            self.t3ch2.pulse_width_percent(abs(self.duty))
        elif self.duty < -100:
            self.IN1_pin.low()
            self.t3ch2.pulse_width_percent(abs(100))
        else:
            pass
    
    
