# -*- coding: utf-8 -*-
"""
@file Controller6.py
@author: Mark Edin
@package Controller6
"""


import utime
import shares6


class Controller:
    """
    @class Controller
    @brief A controller module used to handle the updates of various sensors and set the duty cycle of the motor.
    @details This controller class is meant to be ran as a task in a main.py file located on the nucleo. The controller
    has the MotorDriver, EncoderDriver, and ClosedLoop classes all passed in (through the main.py) so that the controller
    task can make use of them. The class takes a value of Kp and an initial reference velocity, and sets the duty cycle
    to whatever is calculated in the ClosedLoop.update() function in order to reach the desired motor speed. Once the 
    desired speed has been set, shares.done is set to true which triggers a response from the TaskUser.py task. 
    """
    ## Initialization State
    S0_INIT = 0
    ## Check Duty Cycle State
    S1_CHECK_DUTY = 1
    ## Update motor's duty cycle State
    S2_UPDATE_MOTOR = 2
    ## Updates speed values from encoder State
    S3_UPDATE_SPEED = 3
    
    def __init__(self, taskNum, interval, MotorDriver, EncoderDriver, ClosedLoop, dbg=False):
        '''
        @brief This constructor runs when a Controller object is created
        @details The Controller is contructed with an initial task number, interval, MotorDriver, EncoderDriver
        , ClosedLoop, and debug value that are passed in through the main.py that this task is running in. 
        '''
        ## A class copy of the MotorDriver object
        self.MotorDriver = MotorDriver
        ## A class copy of the EncoderDriver object
        self.EncoderDriver = EncoderDriver
        ## A class copy of the ClosedLoop object
        self.ClosedLoop = ClosedLoop
        ## Enabling the motor using the MotorDriver object
        self.MotorDriver.enable() 
        ## The name of the task, used for debugging
        self.taskNum = taskNum 
        ##  The amount of milliseconds between each run of the task. 
        self.interval = int(interval)              
        ## Flag to print debug messages or supress them
        self.dbg = dbg    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()   
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)    
        ## Creates a debug object if 'dbg = True' in the main. 
        if self.dbg:
            print('Created scaler task')
        ## Updating the encoder driver in order to perform a speed calculation on the first run of the task. 
        self.EncoderDriver.update()
        ## A local variable assigned to hold the "real" speed of the motor
        self.Oreal = 0
        ## A local variable object to hold the reference velocity that the task attempts to reach.
        self.Oref = 0
        ## A variable to hold the most current sample of L based on the difference between Oref and Oreal
        self.dutynew = 0
        ## The previous duty cycle calculated by adding dutycurr and duty new (previous run)
        self.dutyold = 0
        ## The duty cycle that the motor will be set to, it is calculated by adding the old duty cycle plus the newly calculated one
        self.dutycurr = 0 
        ## A variable to store the timestamp of a specific speed sample
        self.Otime = 0
        
        
        
        
    def run(self):
        '''
       @brief Runs one iteration of the Controller Task. Waits until the interval has been reached to run again. 
        '''
        
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0: 
            if(self.state == self.S0_INIT):
                self.printTrace()
                self.transitionTo(self.S1_CHECK_DUTY)
            
            elif(self.state == self.S1_CHECK_DUTY):
                self.printTrace()
                self.ClosedLoop.set_Kp(shares6.Kp)
                try:
                    self.Oref = shares6.Oref[0]
                except IndexError:
                    pass
                if self.checkSpeed(self.Oref, self.Oreal) == False:    
                    self.addData(self.Otime, self.Oreal)
                    self.dutynew = self.ClosedLoop.update(self.Oref, self.Oreal)
                    self.dutycurr = self.dutyold + self.dutynew
                    self.dutyold = self.dutycurr
                    shares6.done = False
                    self.transitionTo(self.S2_UPDATE_MOTOR)   
                elif(self.checkSpeed(self.Oref, self.Oreal) == True):
                    shares6.done = True
                     
            elif(self.state == self.S2_UPDATE_MOTOR):
                self.printTrace() 
                self.MotorDriver.set_duty(self.dutycurr)   
                self.transitionTo(self.S3_UPDATE_SPEED)
            
            elif(self.state == self.S3_UPDATE_SPEED):
                self.printTrace()            
                self.EncoderDriver.update()
                self.Oreal = self.EncoderDriver.getSpeed('rad/s')
                self.Otime = utime.ticks_ms()
                self.transitionTo(self.S1_CHECK_DUTY)
            
            else:
                pass
            
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
            
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
    
    def checkSpeed(self, Oref, Oreal):
        '''
        @brief Checks if the current speed is within 0.5 rad/s of the target speed and returns T/F based on if it is or isn't.
        '''
        if abs(Oref-Oreal) <= 0.5: 
            return True 
        elif abs(Oref-Oreal) > 0.5: 
            return False
            
    def addData(self, timedata, speeddata):
        '''
        @brief Appends the most current sample of time and speed data to an array (shares.time/speeddata) that TaskUser.py uses to send back to the front end. 
        '''
        shares6.timedata.append(timedata)
        shares6.speeddata.append(speeddata)            
        