'''
@file shares6.py
@brief A container for all the inter-task variables
@author Mark Edin
@package shares6
'''

## The reference velocity list populated by data received by TaskUser.
Oref     = []
## The Kp variable (gain) populated by the input received by TaskUser.
Kp = 0
## The list that holds the speed data appended by the Controller task to be sent to the Front end by the TaskUser task.
speeddata = []
## The list that holds the time data appended by the Controller Task to be sent to the front end by the TaskUser task.
timedata =[]
## A shared variable used to indicate when the desired speed has been reached by the motor. 
done = False
## A shared variable used to indicate when the data has been sent to the front end. 
sent = False


