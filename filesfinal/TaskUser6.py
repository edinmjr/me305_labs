"""
@file TaskUser6.py
@author Mark Edin
@package TaskUser6
"""

import utime
from pyb import UART
import shares6

class Task_usr:
    '''
    @class Task_usr
    @brief A task class that runs on the nucleo in conjunction with the Controller task. 
    @details This class allows the nucleo to recieve parameters such as Kp and reference speeds from the Front end running on the computer through 
    the serial port. While running, the task waits for the values sent through UART and assigns those values to shared variables located in shares.py. 
    Once the task has shared the input values, it waits until a "Done" command is read from shares.done (set by the Controller Task) and then sends the time
    and speed data back to the computer through the serial port. 
    '''  
    ## Initialization state
    S0_INIT             = 0  
    ## Wait for input variables State
    S1_WAIT_FOR_USR_INPT     = 1 
    ## update Data State
    S2_UPDATE_DATA  = 2

    def __init__(self, taskNum, interval, dbg=False):
        '''
        @brief This constructor runs when a Task_usr object is created
        @details The Task_usr is constructed with an intial task numbner, interval, and debug boolean value.
        '''
        ## The number of the task
        self.taskNum = taskNum 
        ##  The sample frequency of the encoder
        self.interval = int(interval)              
        ## Flag to print debug messages or supress them
        self.dbg = dbg    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT        
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()   
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)    
        ## Class copy of the UART object used to communicate with the UI through the serial port 
        self.myuart = UART(2)          
        ## Debugging
        if self.dbg:
            print('Created scaler task')   
        ## A variable to hold the most recent line read from the serial port
        self.linestring = None
        ## A list used to hold the input variables sent from the Front end on the computer
        self.linelist = []
        ## A local list used to hold the reference variable given by the front end running on the PC
        self.Oref = []
        ## A local list used to store the speed data before it is sent to the PC front end
        self.speeddata = []
        ## A local list used to store the time data before it is sent to the PC front end
        self.timedata = []
        
        
        
    def run(self):
        '''
        @brief Runs one iteration of the TaskUser Task. Waits until the interval has been reached to run again.
        @details The run function is used to run one iteration of the TaskUser task in initialized in the main6.py file. This task waits for a Kp value and reference
        speed to be sent over the serial port, and assigns them to their respective local variables. The variables are then assigned to a shared file where Controller6
        task is able to use them to set the duty cycle of the motor to the intended speed. Once a "done" indication has been made by the Controller6 task, the TaskUser6
        task appends the shared data collected by the Controller6 task to local lists, and then sends the data through the serial port in the form (t,v).
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:   
            if(self.state == self.S0_INIT):
                self.printTrace()                
                #run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_USR_INPT)   
                
            elif(self.state == self.S1_WAIT_FOR_USR_INPT):
                self.printTrace()
                #Run state 1 code
                shares6.sent = False
                shares6.done = False
                if self.myuart.any() != 0:
                    self.linestring = self.myuart.readline().decode('ascii')
                    self.linelist = self.linestring.strip('\r\n').split(',')
                    #print(self.linelist)
                    self.Kp = (self.linelist[0])                   
                    self.Oref = (self.linelist[1])
                    shares6.Kp = float(self.Kp)
                    shares6.Oref.append(int(float(self.Oref)))
                    
                    self.line_list = None
                    self.transitionTo(self.S2_UPDATE_DATA)
                        
            elif(self.state == self.S2_UPDATE_DATA):
                self.printTrace()         
                #state 2 data here:                                                   
                if shares6.done == True:
                    self.updateData(shares6.timedata, shares6.speeddata)
                    for n in range(len(self.speeddata)):
                        print('{:},{:}'.format(self.timedata[n], self.speeddata[n]))
                    shares6.sent = True
                if shares6.sent == True:
                    self.transitionTo(self.S1_WAIT_FOR_USR_INPT)  
                    shares6.sent = False
            else:
                pass
 
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
            
           
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param[in] New state is the state that the current state will transition to when the function is called. 
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
           
            ##Use this to iterate thru Oref array when u get to that point
    def updateData(self, timedata, speeddata):    
        '''
        @brief Updates the local time and speed data lists with the completed time and speed data lists located in shares.py
        @param[in] The timedata and speeddata input parameters should be lists containing all of the collected data from the Controller6 task.
        '''
        self.timedata = timedata
        self.speeddata = speeddata
        
        
    
        
        
        
        