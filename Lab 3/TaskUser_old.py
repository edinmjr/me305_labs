# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 18:02:17 2020

@author: edinm
"""


import pyb, utime
from pyb import UART
from nucencoder import EncoderDriver 



class TaskUser1:
    

    S0_INIT = 0
    S1_WAITING4INPUT = 1
    S2_PROCESSINGINPUT = 2
    
    
    #z needs to zero encoder position (EncoderDriver.zero())
    #p needs to print encoder position (EncoderDriver.getPosistion())
    #d needs to print out encoder delta (EncoderDriver.getDelta())
    
    
    
    
    def __init__(self, interval, EncoderDriver):
        
       
        self.state = self.S0_INIT
        self.runs = 0
        self.start_time = utime.ticks_us()
        self.interval = int(interval)
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        self.curr_time = 0
        self.zBut = 0
        self.pBut = 0
        self.dBut = 0
        self.ser = UART(2)
        self.EncoderDriver = EncoderDriver
        self.uinput = 0
        
        
    
    
    
    
    def run(self):
        
        self.curr_time = utime.ticks_us()
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                print('In state 0')
                self.transitionTo(self.S1_WAITING4INPUT)
                print('transitioning to state 1')
                
            elif(self.state == self.S1_WAITING4INPUT):
                print('In state 1')
                #Run state 1 code
                # self.uinput = input("Enter a command: ")
                print('PLease enter a command, they are p, z, or d:  ')
                
                if self.uinput in ('d', 'p', 'z'):
                    
                    self.next_time = utime.ticks_add(self.next_time, self.interval)
                    self.runs += 1
                    self.transitionTo(self.S2_PROCESSINGINPUT)
                    
 
            elif(self.state == self.S2_PROCESSINGINPUT):
                print('In state 2')
                #Run state 2 code
                
                if(self.uinput == 'z'):
                    #stuff
                    
                    self.EncoderDriver.update()
                    self.EncoderDriver.zero()
                    self.next_time = utime.ticks_add(self.next_time, self.interval)
                    self.runs += 1
                    self.transitionTo(self.S1_WAITING4INPUT)
                elif(self.uinput == 'p'):
                    #stuff
                    self.EncoderDriver.update()
                    self.EncoderDriver.getPosition()
                    self.next_time = utime.ticks_add(self.next_time, self.interval)
                    self.runs += 1
                    self.transitionTo(self.S1_WAITING4INPUT)
                elif(self.uinput == 'd'):
                    #stuff
                    self.EncoderDriver.update()
                    self.EncoderDriver.getDelta()
                    self.next_time = utime.ticks_add(self.next_time, self.interval)
                    self.runs += 1
                    self.transitionTo(self.S1_WAITING4INPUT)
                else:
                    pass
                    
                    
                    
                    
                    
                    
                #     self.transitionTo(self.S1_WAITING4INPUT)
                   
                
                
                
                # self.transitionTo(self.S1_WAITING4INPUT)
                
            
            
            
            
            
        else:
            pass
        
        
            
        # Specifying the next time the task will run
        
    
    
    
    
    # def readInput(self, usersinput):
        
    #     self.uinput = (input("Enter a user command:"))
                
    #     if self.uinput.readchar:
    #         self.pBut = True
    #     elif self.uinput in ('d'):
    #         self.dBut = True
    #     elif self.uinput in ('z'):
    #         self.zBut = True
    #     else:
    #         False
    
    
    
    
    
    def transitionTo(self, NewState):
        self.state = NewState
    
    

