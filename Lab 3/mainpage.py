## @file mainpage.py
#
#
#
#   @mainpage
#   
#   @section sec_intro Introduction
#   This is my website hosting all of my ME305 labs & Homework!
#
#
#   @page page_testing Testing
#
#   @section sec_testing Firmware Testing
#   Text contents.... We can refer to other sections like @ref sec_intro
#
#   @page page_fibonacci Fibonacci
#
#   @section page_fib_src Source Code Access
#   You can find the source code for the fibonacci program here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab1/FibonacciCalc.py
#
#   @section page_fib_doc Documentation
#   The documentation for the fibonacci program is located at FibonacciCalc.Fibonacci.
#
#   @page page_Hw0x00 Homework1
#
#   @section page_Homework1_src Source Code Access
#   Here is the FSM Diagram I made to use while coding TaskElevator:
#   @image html ElevatorFSM_Hw0x00.png
#
#   You can find the source code for the TaskElevator program here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Hw0x00/Hw0x00TaskElevator.py
#
#
#   @section page_Homework1_doc Documentation
#   The documentation for the TaskElevator homework assignment "Hw0x00" is located at  hw0x00TaskElevator.Homework1.
#
#
#   @page page_Lab2b Lab2b
# 
#   @section page_Lab2b_src Source Code Access
#
#   You can find the source code for the two LED Tasks here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/lab2b/nucleoclasseslab2.py
#
#   You can find the source code for the main.py here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/lab2b/nucleomain.py
#
#
#   @section page_Homework1_doc Documentation
#   The documentation for the Lab2b  is located at  nucleoclasseslab2.Lab2b.
#
#
#   @page page_Lab4 Lab4
#
#   @section page_Lab4_src Sorce Code Access
#
#   You can find the source code for Lab4's encoder class driver here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/Lab4/
#
#   @section page_Lab4_doc Documentation
#   The documentation for Lab4 is located at nucencoder.Lab4
#
#
#   @page page_Actual_Lab4 Actual Lab4
#
#   @section Files for collecting data from encoder on motor using the Nucleo 
#   You can find the source code for the encoder data generator program to be placed on Nucleo here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/ActualLab4/UI_dataGen.py

#   You can find the source code for the PC side UI to collect and output encoder data here:
#   https://bitbucket.org/edinmjr/me305_labs/src/master/ActualLab4/UI_FrontLab4.py
#
#   @section page_Actual_Lab4
#   The documentation for the UI Data Gen and UI Front programs is located in
#   the files folder labled UI_dataGen.py and UI_FrontLab4.py. 