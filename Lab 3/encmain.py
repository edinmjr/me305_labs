# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 12:58:30 2020

@author: edinm
"""


from nucencoder import EncoderTask, EncoderDriver
from TaskUser import TaskUser1




import pyb
import shares

PB6 = pyb.Pin.cpu.B6
PB7 = pyb.Pin.cpu.B7
tim = pyb.Timer(4, prescaler = 0, period = 0xffff)
#Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PB6, mode = pyb.Timer.ENC_AB)      
#Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PB7, mode = pyb.Timer.ENC_AB)
##EncoderDriver Obj
Encoder = EncoderDriver(PB6, PB7, tim, 0xffff)


#Task Obj
task0 = EncoderTask(1000, Encoder)
task1 = TaskUser1(1, 10000, dbg = True)

#To run the task call task1.run() over and over

tasklist = [task0,
            task1]

while True:
    for task in tasklist:
        task.run()