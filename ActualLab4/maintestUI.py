'''
@file main.py
@brief Used to run the UI front for Lab4. 
@details Tasks imported and present in the task list are ran continuously. 
'''

from UI_FrontLab4 import UI_Task   


## Task object
task0 = UI_Task(0, 1, dbg=False)
## Task list holding tasksgsgsgs
taskList = [task0]


while True:
    for task in taskList:
        task.run()
    