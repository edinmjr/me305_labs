## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Your Name
#
#  @copyright License Info
#
#  @date January 1, 1970
#
#  @package encoder
#  Brief doc for the encoder module
#
#  Detailed doc for the encoder module
#
#  @author Your Name
#
#  @copyright License Info
#
#  @date January 1, 1970
import utime
import pyb
import shares
from pyb import UART 
## An encoder driver object
#
#  Details
#  @author Your Name
#  @copyright License Info
#  @date January 1, 1970
class EncoderDriver:
    '''
    @brief A class that allows the encoder to be ran
    @details 
    '''
    
	## Constructor for encoder driver
	#
	#  Detailed info on encoder driver constructor
    #channel info, set timers, zero var.
    def __init__(self, PA6, PA7, tim, period):
        
      
        
        self.period = 0xffff
        self.PA6 = pyb.Pin.cpu.A6
        self.PA7 = pyb.Pin.cpu.A7
        self.mode = pyb.Timer.ENC_AB
        self.count = 0
        self.prev_count = 0
        self.delta = 0
        self.gdelta = 0
        self.position = 0
        self.m_position = 0 
        self.tim = pyb.Timer(3)
        self.tim.init(prescaler = 0, period = self.period)
        
        
        
        #Channel 1 (A) timer
        #Ch1 = tim.channel(1, pin = self.PA6, mode = self.mode)
        
        #Channel 2 (B) timer
       # Ch2 = tim.channel(2, pin = self.PA7, mode = self.mode)
        
        
	## Updates the recorded position of the encoder
	#
	# Detailed info on encoder update method
    #handling good/bad delta
    def update(self):
		
        self.prev_count = self.count
        self.count = self.tim.counter()
        
        if abs(self.count - self.prev_count) > self.period/2:
            
            self.delta = self.count - self.prev_count
            if self.delta > 0:
                self.gdelta = self.delta - self.period
                
            elif self.delta < 0:
                self.gdelta = self.delta + self.period
            
        elif abs(self.count - self.prev_count) < self.period/2:
            self.delta = self.count - self.prev_count
            self.gdelta = self.delta
            
        else:
            self.gdelta = 0
        
        self.m_position += self.gdelta
        shares.resppos = self.m_position
        shares.resdelt = self.delta
        
        
    
	## Gets the encoder's position
	#
	#  Detailed info on encoder get_position method
    #return last "update" call
    def getPosition(self):
        
        return (self.m_position)
        
    ##Resets the position to a specified value
    #
    # Detailed info on encoder set_position method
    #set motor position to a specified value
    def setPosition(self, position):
        '''
        @brief
        '''
        self.m_position = position
        
    
    
    ## Returns the difference in recorded position between the two most recent calls to update()
    #
    # Detailed info on encoder get_delta method
    #difference in recent update() calls
    def getDelta(self):
        '''
        @brief
        '''
        return self.delta
    
	## Zeros out the encoder
	#
	#  Detailed info on encoder zero function
    def zero(self):
        '''
        @brief
        '''
        self.m_position = 0
        return (self.m_position)
        
        
        
        
class EncoderTask:
    '''
    @brief
    @details
    '''
    S0_INIT = 0
    S1_RUNNING = 1
    
    
    def __init__(self, interval, EncoderDriver):
        '''
        @brief
        '''
        
        self.EncoderDriver = EncoderDriver
        
        self.state = self.S0_INIT
        
        self.start_time = utime.ticks_us()
          ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval)
          ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        self.curr_time = 0
        
        self.ser = UART(2)
        
        

        
    def run(self):
        
        self.curr_time = utime.ticks_us()
        
        if (self.state == self.S0_INIT):
            print('Tranisitioning to S1_RUNNING')
            self.transitionTo(self.S1_RUNNING)
            
        elif (self.state == self.S1_RUNNING):
            
            
            if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                self.EncoderDriver.update()
                self.EncoderDriver.getPosition()
                self.next_time += self.interval
            
            
        else:
            
            pass
        
     
    def transitionTo(self, NewState):
        self.state = NewState
    
