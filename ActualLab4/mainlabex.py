# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:23:09 2020

@author: edinm
"""

from pyb import UART

myuart = UART(2)

while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('\n you sent an ASCII ' + str(val) + ' to the Nucleo ')