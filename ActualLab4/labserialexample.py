# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:20:12 2020

@author: edinm
"""

import serial 
ser = serial.Serial(port='COM3', baudrate = 115273, timeout = 1)

def sendChar():
    inv = input('Give me a character:  ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval
    



for n in range(1):
    print(sendChar())
    
ser.close()

#TO SAVE STUFF AS CSV FILE:

    #import numpy
    #import numpy as np
    #thing = np.array([1, 0, 5, 5, 5])
    #np.savetxt('somefilename.csv', thing, delimiter=",')
    #^^file will be saved to the folder the script is saved in