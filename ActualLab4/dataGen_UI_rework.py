# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 09:20:47 2020

@author: edinm
"""

#import shares
#from nucencoder import EncoderDriver
import utime
import array as arr
from pyb import UART
#import serial
import shares



class UI_dataGenTask:
    '''
    @brief
    @details
    '''
    
     ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_WAIT_FOR_USR_INPT     = 1
    
    ## Process User Input
    S2_PROCESS_INPT  = 2
    
    
    
    
    
    def __init__(self, taskNum, interval, EncoderDriver, dbg=True):
        
        ## The name of the task
        self.taskNum = taskNum
        ##  The interval between runs of the task
        self.interval = int(1E6/interval)
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT       
        ## Counter that describes the number of times the task has run
        self.runs = 0        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()    
         ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)        
        if self.dbg:
            print('Created scaler task')            
            
        self.posArray = arr.array('l', [])
        
        self.timArray = arr.array('l', [])
        
        
        self.position = 0
        
        self.EncoderDriver = EncoderDriver
        
        self.idx = 0
        
        self.next_timeSam = utime.ticks_add(self.start_time, self.interval)
        self.sample_time = None
        
        self.myuart = UART(2)
            
        
            
    def runUI(self, interval):
        '''
        Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:   
            if(self.state == self.S0_INIT):
                self.printTrace()                
                #run state 0 code
                self.transitionTo(self.S1_WAIT_FOR_USR_INPT)
                
            elif(self.state == self.S1_WAIT_FOR_USR_INPT):
                self.printTrace()
                #Run state 1 code
                #if command waiting on bus
                if self.myuart.any() != 0:
                    #checks what char is in bus
                    shares.cmd = self.myuart.readchar()
                    if(shares.cmd == 115):
                        #if datagen is requested to end
                        #clear cmd
                        shares.cmd = None
                        #return to prev state 
                        self.transitionTo(self.S0_INIT)
                        #tell user that data coll ended early due to usr cmd
                        shares.resp = 'Terminating data collection early due to user input'
                        #clear resp
                        shares.resp = None
                        #send data collected to comp
                        for n in range(len(self.posArray)):
                            print('{:},{:}'.format(self.timArray[n], self.posArray[n]))
                        
                        
                    elif(shares.cmd == 103):
                        #if datagen is requested to start
                        #clear cmd 
                        shares.cmd = None
                        #return to previous state 
                        self.transitionTo(self.S0_INIT)
                        #tell user that data coll is starting
                        shares.resp = 'Beginning Data Collection'
                        #clear resp
                        shares.resp = None
                        self.runs += 1
                        # Specifying the next time the task will run
                        self.next_time = utime.ticks_add(self.next_time, self.interval)
                        
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                        


            # elif(self.state == self.S2_COLL_DATA):
            #     self.printTrace()
            #     #state 2 data here:
            #     if(self.cmd == 115):
            #         self.coltime = utime.ticks_ms()
            #         print("Beginning Data Collection")
            #         while(self.idx < 10*interval and self.cmd != 115 and self.cmd == 103):                           
            #             self.sample_time = utime.ticks_ms()
            #             if(utime.ticks_diff(self.sample_time, self.next_timeSam) >= 0):
            #                 self.samplePos()   
            #                 self.posArray.append(self.position)
            #                 self.timArray.append((self.sample_time)*1)
            #                 self.idx += 1                                                           
            #                 if self.myuart.any() != 0:
            #                     self.cmd = self.myuart.readchar()
            #                 self.next_timeSam = utime.ticks_add(self.next_timeSam, self.interval)
                                
                            
            # elif(self.state == self.S3_SEND_DATA):
            #     self.printTrace()
            #     #state 3 code here:
            #     print("Data collection terminated early, sending data")
            #         for n in range(len(self.posArray)):
            #             print('{:},{:}'.format(self.timArray[n], self.posArray[n]))
            #             self.transitionTo(self.S0_INIT)
            
            
            # elif(self.state == self.S4_CLEAR_DATA):
            #     self.printTrace()
            #     #state 4 code here:
            #     self.EncoderDriver.zero()
            #     self.posArray = []
            #     self.timArray = []
            #     self.idx = 0
            #     self.transitionTo(self.S1_WAIT_FOR_USR_INPT)
                
            # self.runs += 1
            # # Specifying the next time the task will run
            # self.next_time = utime.ticks_add(self.next_time, self.interval)  
                    
                    
        
    
    def samplePos(self):
        self.EncoderDriver.update()
        self.position = self.EncoderDriver.getPosition()
        
    
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
            
            
            
            

class dataGenTask:
    '''
    @brief
    @details
    '''
    ## Initialization state
    S0_INIT             = 0
    
    ## Wait for command state
    S1_COLLECT_DATA     = 1
    
    ## Collect Data
    S2_COLL_DATA  = 2
    
    ## Send Data
    S3_SEND_DATA = 3
    
    ##Clear Storage
    S4_CLEAR_DATA = 4
    
    
    def __init__(self, taskNum, interval, EncoderDriver, dbg=True):
        
        ## The name of the task
        self.taskNum = taskNum
        
        ##  The sample frequency of the encoder
        self.interval = int(1E3/interval)
        
        shares.idx = interval*10
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
    
         ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created scaler task')
            
            
        self.posArray = arr.array('l', [])
        
        self.timArray = arr.array('l', [])
        
        
        self.position = 0
        
        self.EncoderDriver = EncoderDriver
        
        self.idx = 0
        
        self.next_timeSam = utime.ticks_add(self.start_time, self.interval)
        self.sample_time = None
        
        self.myuart = UART(2)
    
    def runDG(self, interval):
        '''
        Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:   
            if(self.state == self.S0_INIT):
                self.printTrace()                
                #run state 0 code
                self.transitionTo(self.S1_COLLECT_DATA)
                
            elif(self.state == self.S1_COLLECT_DATA):
                self.printTrace()
                #Run state 1 code
                
                           

              
                    self.coltime = utime.ticks_ms()
                    print("Beginning Data Collection")
                    while(self.idx < 10*interval and self.cmd != 115 and self.cmd == 103):                           
                        self.sample_time = utime.ticks_ms()
                        if(utime.ticks_diff(self.sample_time, self.next_timeSam) >= 0):
                            self.samplePos()   
                            self.posArray.append(self.position)
                            self.timArray.append((self.sample_time)*1)
                            self.idx += 1                                                           
                            if self.myuart.any() != 0:
                                self.cmd = self.myuart.readchar()
                            self.next_timeSam = utime.ticks_add(self.next_timeSam, self.interval)
                                
                            
            elif(self.state == self.S3_SEND_DATA):
                self.printTrace()
                #state 3 code here:
                print("Data collection terminated early, sending data")
                    for n in range(len(self.posArray)):
                        print('{:},{:}'.format(self.timArray[n], self.posArray[n]))
                        self.transitionTo(self.S0_INIT)
            
            
            elif(self.state == self.S4_CLEAR_DATA):
                self.printTrace()
                #state 4 code here:
                self.EncoderDriver.zero()
                self.posArray = []
                self.timArray = []
                self.idx = 0
                self.transitionTo(self.S1_WAIT_FOR_USR_INPT)
                
            self.runs += 1
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)  
                    
                    
        
    
    def samplePos(self):
        self.EncoderDriver.update()
        self.position = self.EncoderDriver.getPosition()
        
    
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, self.curr_time)
            print(str)
    