# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 09:06:55 2020

@author: edinm
"""

import shares
import utime
from pyb import UART



class UI_Front:
    '''
    @brief
    @details
    '''
    
    
    def __init__(self):
        '''

        Returns
        -------
        None.

        '''
        
        #Serial Port
        self.ser = UART(2)
        
        
    
    def sendChar(self):
        inv = input('Give me a char')
        self.ser.write(str(inv)).encode('ascii')
        myval = self.ser.readline().decode('ascii')
        return myval
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
        
    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)