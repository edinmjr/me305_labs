'''
@file shares.py
@brief A container for all the inter-task variables
@author Charlie Refvem
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''
import array as arr

## The command character sent from the user interface task t
cmd     = None

## The response from the encoder task after receiving command
resp    = None

posArray = arr.array('l', [])
        
timArray = arr.array('l', [])