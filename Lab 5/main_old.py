# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 08:32:48 2020

@author: edinm
"""
from Lab5classes import BLEModuleDriver   
from LEDController import LEDController
from pyb import UART
import pyb
import utime

## UART object
myuart = UART(3, baudrate = 9600)  
  
## Pin Object
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## Freq of runs of the task
interval = 1

## Task object
task0 = BLEModuleDriver(0, interval, dbg=True)
task1 = LEDController(1, interval, dbg=True)
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run(interval)

# class lab5ex:
    
#     def __init__(self):
        


        
        
        
        
        
