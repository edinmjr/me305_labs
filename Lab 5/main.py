# -*- coding: utf-8 -*-
"""
@file main.py
@brief A main.py file to run on the nucleo holding the BLE and LED controller tasks
@details This main.py file creates 2 objects, and sets them to individual tasks. Each task runs seperate from eachother
and allows the LED controller to run while the BLEModuleDriver task is running. 
@author Mark Edin
"""

from LEDController import LEDController 
from Lab5classes import BLEModuleDriver

interval = 800

## Task objects
task0 = BLEModuleDriver(0, interval, dbg=True)
task1 = LEDController(1, interval, dbg=True)
## Task List
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()
        #task.run()
        
    
