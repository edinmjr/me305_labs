"""
@file main.py
@brief A file used to intialize class objects and run tasks simultainiously. 
@details This main.py initializes the Encoder object (with the correct pins and timers), initializes the 
Motor object (with the correct pins and timer), and initializes the closed loop object with an intial value of 0.1,
it is changed later by the input given through the front end. 
@author Mark Edin
"""
from TaskUser import Task_usr   
from nucencoder import EncoderDriver
from MotorDriver import MotorDriver
from Controller import Controller
from ClosedLoopModule import ClosedLoop
import pyb
import sys


    
## Encoder Object
PC6 = pyb.Pin.cpu.C6
PC7 = pyb.Pin.cpu.C7
tim = pyb.Timer(8, prescaler = 0, period = 0xffff)
#Channel 1 (A) timer
Ch1 = tim.channel(1, pin = PC6, mode = pyb.Timer.ENC_AB)      
#Channel 2 (B) timer
Ch2 = tim.channel(2, pin = PC7, mode = pyb.Timer.ENC_AB)
##EncoderDriver Obj
Encoder = EncoderDriver(PC6, PC7, tim, 0xffff)

## Motor Object
#Sleep Pin
nSLEEP_pin = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pinB4 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pinB4.low()
pinB5 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
pinB5.low()
## Pin B4
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
#pinB4.low()
## Pin B5
pinB1 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
pinB1.low()
timer= pyb.Timer(3, freq = 20000)
#MOtor driver obj
Motor = MotorDriver(nSLEEP_pin, pinB0, pinB1, timer)

ClosedLoop = ClosedLoop(0.1)

interval = 30

## Task object
task0 = Controller(0, interval, Motor, Encoder, ClosedLoop, dbg=False)
task1 = Task_usr(1, interval, dbg=False)
taskList = [task0,
            task1]
## A try/except case to completely kill the motors after a keyboard interrupt in REPL.
try:
    while True:
        for task in taskList:
            task.run()
except KeyboardInterrupt:
    print("Shutting Down")
    sys.exit()
    
