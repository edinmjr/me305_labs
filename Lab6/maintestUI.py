'''
@file maintestUI.py
@brief Used to run the UI front for Lab6. 
@details Tasks imported and present in the task list are ran continuously. 
'''

from UI_FrontLab6 import UI_Task   

## Task object
task0 = UI_Task(0, 2, dbg=True)
## Task list holding tasks
taskList = [task0]

while True:
    for task in taskList:
        task.run()
