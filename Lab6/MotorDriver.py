"""
@file MotorDriver.py
@author Mark Edin
"""

import pyb

class MotorDriver:
    '''
    @brief A module used to control the motors on our breakout board.
    @details This class allows us to control the speed and direction of the motors on our breakout board.
    '''
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''
        @brief This constructor runs when a MotorDriver object is created
        @details This MotorDriver takes in passed variables for the Sleep pin, IN1 pin, IN2 pin, and the timer associated
        with the specfic motor being used. 
        '''
        ## Sleep Pin object
        self.sleep_pin = nSLEEP_pin
        self.sleep_pin.low()
        
     
        self.IN1_pin = IN1_pin
        
        ## IN4 pin object
        self.IN2_pin = IN2_pin
        
        ## Timer object
        self.timer = timer
        
        self.t3ch1 = self.timer.channel(3, pyb.Timer.PWM, pin = IN1_pin)
        # ## Channel 4 Object
        self.t3ch2 = self.timer.channel(4, pyb.Timer.PWM, pin = IN2_pin)
    
    def enable(self):
        '''
        @brief This method enables the motor by setting the sleep pin to high. 
        '''
        self.sleep_pin.high()
        #print('Enabling Motor')
        
        
    def disable(self):
        '''
        @brief This method disables the motor by setting the sleep pin to low. 
        '''
        self.sleep_pin.low()
        #print('Disabling Motor')
        
    def set_duty(self, duty): 
        '''
        @brief Sets the duty cycle of the motor to whatever was given to the input parameter "duty". There is a 
        failsafe that makes sure there is never more than 100% effort requested from the motor, even if the requested effort
        is negative or positive. 
        '''      
        self.duty = duty
        if self.duty >= 0 and self.duty <= 100:
            self.t3ch1.pulse_width_percent(self.duty)
            self.IN2_pin.low()
        elif self.duty < 0 and self.duty >= -100:
            self.IN1_pin.low()
            self.t3ch2.pulse_width_percent(abs(self.duty))
        else:
            pass
    
    
   
        