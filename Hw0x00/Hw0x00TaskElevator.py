
'''
@file hw0x00TaskElevator.py
@brief A file that utilizes classes to virtualize an elevator
@author: edinm
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of the virtual elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1
    
    ## Constant defining State 2
    S2_MOVING_UP = 2
    
    ## Constant defining State 3
    S3_STOPPED_AT_FLOOR_1 = 3
    
    ## Constant defining State 4
    S4_STOPPED_AT_FLOOR_2 = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING = 5
    
    def __init__(self, interval, Motor, button_1, button_2, First, Second,):
        '''
        @brief      This constructor runs when a TaskElevator object is created
        @details The TaskElevator is contructed with no initial conditions
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the Button_1 object
        self.button_1 = button_1
        
        ## A class attribute "copy" of the Button_2 object
        self.button_2 = button_2
        
        ## The button object used for the right limit
        self.First = First
        
        ## The button object used for the right limit
        self.Second = Second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        @details    Runs one iteration of the TaskElevator. While runnning, it outputs information
        like what state the elevator is in, what floor it's on, if its moving, what direction the 
        motor is moving, or if the motor is stopped. It also includes the timestamps of each state
        transition. 
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))  
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Down()
                self.button_1.getButtonState(2)
                self.button_2.getButtonState(2)
                self.First.getButtonState(1)
                
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                if(self.First.getButtonState(1)):
                    self.transitionTo(self.S3_STOPPED_AT_FLOOR_1)
                    self.Motor.Stop()
                    print('Elevator Stopped at Floor 1')
                    
                
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                if(self.Second.getButtonState(1)):
                    self.transitionTo(self.S4_STOPPED_AT_FLOOR_2)
                    self.Motor.Stop()
                    print('Elevator Stopped at Floor 2')
                    
                    
            elif(self.state == self.S3_STOPPED_AT_FLOOR_1):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                    # Run State 3 Code   
                if (self.button_2.getButtonState(0)):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                    #to make sure the state doesn't change if button_1 is pressed while on floor 1
                elif self.button_1.getButtonState(0):
                    self.transitionTo(self.S3_STOPPED_AT_FLOOR_1)
                    
            elif(self.state == self.S4_STOPPED_AT_FLOOR_2):
                self.Second.getButtonState(1)
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if (self.button_1.getButtonState(0)):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                    #to make sure the state doesn't change if button_2 is pressed while on floor 2
                elif self.button_2.getButtonState(0):
                    self.transitionTo(self.S4_STOPPED_AT_FLOOR_2)
      
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
                
            
            else:
                # Uh-oh state (undefined state)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
            '''
            @brief      Updates the variable defining the next state to run
            '''
            self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))
        
 
    def getButtonState(self, btnVal):
        '''
        @brief      Gets the button state and also sets the state of the button. 
        @details    Since there is no hardware attached this method
                    btnVal == 0 returns a randomized True or False value
                    btnVal == 1 returns a True Value
                    btnVal == 'Anything Else' returns a False value
        @return     A boolean representing the state of the button.
        '''
        if btnVal == 0:
            return choice([True, False])
        elif btnVal == 1:
            return True
        else:
            return False
    
        

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used move the elevator up and down. 
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor up
        '''
        print('Motor moving Up')
    
    def Down(self):
        '''
        @brief Moves the motor down
        '''
        print('Motor moving down')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Stopped')

##Motor Obj
Motor = MotorDriver()
button_1 = Button('PB6')
button_2 = Button('PB7')
First = Button('PB8')
Second = Button('PB9')


#Task Obj
task1 = TaskElevator(0.1, Motor, button_1,button_2, First, Second)
task2 = TaskElevator(0.2, Motor, button_1,button_2, First, Second)

#To run the task call task1.run() over and over

for N in range(100000000): #Will change to "while True:" when we're on hardware
    task1.run()
    task2.run()
    #task3.run()