"""
@file nucleomain.py
@author Mark Edin
"""
from nucleoclasseslab2 import vLEDFSM
from nucleoclasseslab2 import vLED
from nucleoclasseslab2 import rLEDFSM
import pyb


    
## Pin Object
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## Interval between runs of the task
interval = 500

## Task object
task0 = vLEDFSM(interval, vLED)
task1 = rLEDFSM(interval)
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()

    